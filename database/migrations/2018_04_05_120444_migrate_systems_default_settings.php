<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MigrateSystemsDefaultSettings extends Migration
{

    public function up()
    {
        Schema::create('admins', function(Blueprint $table){
            $table->string('id')->unique();
            $table->string('user_name')->nullable();
            $table->string('password')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('full_name')->nullable();
            $table->string('account_number')->nullable();
            $table->string('account_network')->nullable();

            $table->timestamp('last_logged_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('configurations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedDecimal('factor')->nullable();
            $table->unsignedSmallInteger('maturity_period')->nullable();
            $table->unsignedSmallInteger('amount_decimal_places')->nullable();
            $table->unsignedDecimal('first_percentage')->nullable();
            $table->unsignedDecimal('remaining_percentage')->nullable();
            $table->unsignedSmallInteger('pre_maturity_period')->nullable();
            $table->unsignedSmallInteger('allow_recommitment_period')->nullable();
            $table->unsignedSmallInteger('transaction_lap_hours')->nullable();

            $table->unsignedSmallInteger('minimum_donation')->nullable();
            $table->unsignedSmallInteger('maximum_donation')->nullable();
            $table->timestamps();
        });

        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });

        Schema::create('sessions', function (Blueprint $table) {
            $table->string('id')->unique();
            $table->unsignedInteger('user_id')->nullable();
            $table->string('ip_address', 45)->nullable();
            $table->text('user_agent')->nullable();
            $table->text('payload');
            $table->integer('last_activity');
        });

    }


    public function down()
    {
        $tables = [
            'admins', 'configurations', 'password_resets', 'sessions'
        ];

        foreach ($tables as $t) {
            Schema::dropIfExists($t);
        }
    }
}
