<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MigrateAllBlueprints extends Migration
{

    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');

            $table->string('user_name')->nullable();
            $table->string('email')->nullable();
            $table->string('password')->nullable();
            $table->string('region')->nullable();
            $table->string('phone')->nullable();

            $table->string('payment_name')->nullable();
            $table->string('payment_method')->nullable();
            $table->string('payment_number')->nullable();

            $table->boolean('is_confirmed')->default(0);
            $table->boolean('is_barred')->default(0);
            $table->unsignedSmallInteger('referred_by')->nullable();

            $table->timestamp('last_logged_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('complaints', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reason')->nullable();
            $table->longText('comment')->nullable();
            $table->boolean('is_resolved')->default(false);
            $table->timestamps();
            $table->unsignedInteger('user_id')->index();
        });

        Schema::create('donations', function(Blueprint $table){
            $table->increments('id');

            $table->dateTime('queue_date');
            $table->boolean('is_donor')->default(0);
            $table->boolean('is_receiver')->default(0);

            $table->string('account_status')->nullable();
            $table->string('action_button_status')->nullable();

            $table->boolean('initial_paid')->default(0);
            $table->boolean('remnant_paid')->default(0);
            $table->boolean('fully_paid')->default(0);

            $table->integer('init_amount')->nullable();
            $table->integer('remnant_amount')->nullable();

            $table->string('deposited_amount')->nullable();
            $table->string('current_amount')->nullable();
            $table->string('growth_rate')->nullable();
            $table->string('expected_return_amount')->nullable();
            $table->dateTime('current_donation_date')->nullable();

            $table->unsignedInteger('user_id')->index();
            $table->timestamps();
        });

        Schema::create('donors', function(Blueprint $table){

            $table->increments('id');
            $table->unsignedInteger('donation_id');
            $table->unsignedInteger('user_id');
            $table->integer('total_amount');
            $table->integer('amount_left');
            $table->boolean('is_remnant')->nullable(); // true => remaining amount, false => initial amount

            $table->timestamps();
        });

        Schema::create('sacrifices', function(Blueprint $table){

            $table->increments('id');
            $table->unsignedInteger('donation_id');
            $table->unsignedInteger('user_id');
            $table->integer('total_amount');
            $table->boolean('is_part')->default(1);  // true => first partial amount...used to maintain system costs and expenses

            $table->timestamps();
        });

        Schema::create('beneficiaries', function(Blueprint $table){

            $table->increments('id');
            $table->unsignedInteger('donation_id');
            $table->unsignedInteger('user_id');
            $table->integer('total_amount');
            $table->integer('amount_left');
            $table->boolean('is_complete')->default(0); // Flags if recipient has fully received the help

            $table->timestamps();

        });

        Schema::create('matches', function (Blueprint $table) {
            $table->increments('id');

            $table->boolean('has_donor_paid')->default(false);

            $table->string('sender_id')->nullable();
            $table->string('sender_number')->nullable();
            $table->string('sender_name')->nullable();

            $table->string('receiver_id')->nullable();
            $table->string('receiver_phone')->nullable();
            $table->string('receiver_account_number')->nullable();
            $table->string('receiver_account_network')->nullable();
            $table->string('receiver_name')->nullable();

            $table->boolean('payment_confirmed')->default(0);
            $table->boolean('payment_declined')->default(0);
            $table->dateTime('payment_completed_by');
            $table->string('amount')->nullable();

            $table->boolean('is_remnant')->default(false);
            $table->string('proof_of_payment')->nullable();
            $table->timestamps();
        });
    }


    public function down()
    {
        $tables = [
            'users', 'complaints', 'donations', 'donors', 'beneficiaries', 'matches', 'sacrifices'
        ];

        foreach ($tables as $t) {
            Schema::dropIfExists($t);
        }
    }
}
