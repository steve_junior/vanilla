<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = array(
            [
                'user_name' => 'anita',
                'email' => 'anita@gmail.com',
                'password' => bcrypt('123456'),
                'phone' => '0576567333',
                'region' => 'Accra',
                'payment_name' => 'Anita Preko',
                'payment_method' => 'Tigo Cash',
                'payment_number' => '0576567333',
                'referred_by'    => null,
                'last_logged_at' => now()->toDateTimeString(),
                'created_at' => now()->toDateTimeString()
            ],
            [
                'user_name' => 'jesus',
                'email' => 'jesus@gmail.com',
                'password' => bcrypt('123456'),
                'phone' => '0552515858',
                'region' => 'Accra',
                'payment_name' => 'Afriyie Jesus',
                'payment_method' => 'MTN Mobile Money',
                'payment_number' => '0552515858',
                'referred_by'    => null,
                'last_logged_at' => now()->toDateTimeString(),
                'created_at' => now()->toDateTimeString()
            ],
            [
                'user_name' => 'socrates',
                'email' => 'socrates@gmail.com',
                'password' => bcrypt('123456'),
                'phone' => '0268098992',
                'region' => 'Accra',
                'payment_name' => 'Socrates',
                'payment_method' => 'Airtel Money',
                'payment_number' => '0268098992',
                'referred_by'    => null,
                'last_logged_at' => now()->toDateTimeString(),
                'created_at' => now()->toDateTimeString()
            ],[
                'user_name' => 'david',
                'email' => 'david@gmail.com',
                'password' => bcrypt('123456'),
                'phone' => '0542570855',
                'region' => 'Accra',
                'payment_name' => 'David Kumi',
                'payment_method' => 'MTN Mobile Money',
                'payment_number' => '0542570855',
                'referred_by'    => 1,
                'last_logged_at' => now()->toDateTimeString(),
                'created_at' => now()->toDateTimeString()
            ],[
                'user_name' => 'daniella',
                'email' => 'daniella@gmail.com',
                'password' => bcrypt('123456'),
                'phone' => '0242152408',
                'region' => 'Accra',
                'payment_name' => 'Daniella Nkansah',
                'payment_method' => 'MTN Mobile Money',
                'payment_number' => '0242152408',
                'referred_by'    => 1,
                'last_logged_at' => now()->toDateTimeString(),
                'created_at' => now()->toDateTimeString()
            ],
        );

        foreach ($users as $user)
        {
            DB::table('users')->insert($user);
        }

       $rest = array( [
            'user_name' => 'guest6',
            'email' => 'guest6@gmail.com',
            'password' => bcrypt('123456'),
            'phone' => '0276789044',
            'region' => 'Accra',
            'payment_name' => 'Guest 6',
            'payment_method' => 'Tigo Cash',
            'payment_number' => '0276789044',
            'referred_by'    => 1,
            'last_logged_at' => now()->toDateTimeString(),
            'created_at' => now()->toDateTimeString()
        ],[
        'user_name' => 'guest7',
        'email' => 'guest7@gmail.com',
        'password' => bcrypt('123456'),
        'phone' => '0208990911',
        'region' => 'Accra',
        'payment_name' => 'Guest 7',
        'payment_method' => 'Vodafone Cash',
        'payment_number' => '0208990911',
        'referred_by'    => 2,
        'last_logged_at' => now()->toDateTimeString(),
        'created_at' => now()->toDateTimeString()
    ],[
        'user_name' => 'guest8',
        'email' => 'guest8@gmail.com',
        'password' => bcrypt('123456'),
        'phone' => '0501366265',
        'region' => 'Accra',
        'payment_name' => 'Guest 8',
        'payment_method' => 'Vodafone Cash',
        'payment_number' => '0501366265',
        'referred_by'    => 2,
        'last_logged_at' => now()->toDateTimeString(),
        'created_at' => now()->toDateTimeString()
    ],
            [
                'user_name' => 'guest9',
                'email' => 'guest9@gmail.com',
                'password' => bcrypt('123456'),
                'phone' => '0266147725',
                'region' => 'Accra',
                'payment_name' => 'Guest 9',
                'payment_method' => 'Airtel Cash',
                'payment_number' => '0266147725',
                'referred_by'    => null,
                'last_logged_at' => now()->toDateTimeString(),
                'created_at' => now()->toDateTimeString()
            ],[
        'user_name' => 'guest10',
        'email' => 'guest10@gmail.com',
        'password' => bcrypt('123456'),
        'phone' => '0550109525',
        'region' => 'Accra',
        'payment_name' => 'Guest 10',
        'payment_method' => 'Vodafone Cash',
        'payment_number' => '0550109525',
        'referred_by'    => 7,
        'last_logged_at' => now()->toDateTimeString(),
        'created_at' => now()->toDateTimeString()
    ]
       );
    }
}
