<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    public function run()
    {
        $this->call(AdministratorTableSeeder::class);
        $this->call(ConfigurationTableSeeder::class);
        $this->call(DonationBenefactorSeeder::class);
        $this->call(UsersTableSeeder::class);
    }
}
