<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdministratorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = array(
            'id'             => bcrypt('1'),
            'user_name'      => 'admin',
            'email'          => 'admin@gmail.com',
            'password'       => bcrypt('123456'),
            'phone'          => '087 656 7333',
            'account_number' => '050 656 7333',
            'account_network' => 'Vodafone Cash',
            'full_name'      => 'Steve Junior',
            'last_logged_at' => now()->toDateTimeString(),
            'created_at'     => now()->toDateTimeString()
        );

        DB::table('admins')->insert($admin);
    }
}
