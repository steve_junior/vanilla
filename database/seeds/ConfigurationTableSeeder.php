<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfigurationTableSeeder extends Seeder
{

    public function run()
    {
        $settings = array(
            'factor' => 1.5,
            'maturity_period' => 7,
            'amount_decimal_places' => 6,
            'first_percentage' => 0.4,
            'remaining_percentage' => 0.6,
            'pre_maturity_period' => 6,
            'allow_recommitment_period' => 12,
            'transaction_lap_hours' => 24,
            'minimum_donation' => 100,
            'maximum_donation' => 2000,
            'created_at' => now()->toDateTimeString()
        );

        DB::table('configurations')->insert($settings);
    }
}
