<?php

use App\Helpers\Enums\AccountStatus;
use App\Helpers\Enums\ActionButtonStatus;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class DonationBenefactorSeeder extends Seeder
{

    public function run()
    {
        $DefaultReceiver1 = [
            'user_name' => 'steve',
            'email' => 'steve@gmail.com',
            'password' => bcrypt('123456'),
            'phone' => '057 656 7333',
            'region' => 'Accra',
            'payment_name' => 'Steve Junior',
            'payment_method' => 'Tigo Cash',
            'payment_number' => '057 656 7333',
            'referred_by'    => null,
            'last_logged_at' => now()->toDateTimeString(),
            'created_at' => now()->toDateTimeString()
        ];

        $DefaultReceiver2 = [
            'user_name' => 'jerry',
            'email' => 'jerry@gmail.com',
            'password' => bcrypt('123456'),
            'phone' => '050 777 7333',
            'region' => 'Accra',
            'payment_name' => 'Jerry Cobla',
            'payment_method' => 'Vodafone Cash',
            'payment_number' => '050 777 7333',
            'referred_by'    => '1',
            'last_logged_at' => now()->toDateTimeString(),
            'created_at' => now()->toDateTimeString()
        ];

        DB::table('users')->insert($DefaultReceiver1);
        DB::table('users')->insert($DefaultReceiver2);

        $DefaultReceiverDonation1 = [
            'deposited_amount' => 1000,
            'current_amount' => 1000,
            'expected_return_amount' => 1500,
            'growth_rate' => 71.428571,
            'init_amount' => 400,
            'remnant_amount' => 600,
            'queue_date' => now()->addHours(24)->toDateTimeString(),
            'user_id' => '1',
            'is_donor' => false,
            'is_receiver' => true,
            'initial_paid' => true,
            'remnant_paid' => true,
            'fully_paid' => true,
            'account_status' => AccountStatus::PAID,
            'action_button_status' => ActionButtonStatus::QUEUE,
            'created_at' => now()->subDays(7)->toDateTimeString(),
            'updated_at' => now()->subDays(7)->toDateTimeString(),
            'current_donation_date' => now()->subDays(7)->addHours(24)->toDateTimeString()
        ];

        $DefaultReceiverDonation2 = [
            'deposited_amount' => 1000,
            'current_amount' => 1000,
            'expected_return_amount' => 1500,
            'growth_rate' => 71.428571,
            'init_amount' => 400,
            'remnant_amount' => 600,
            'queue_date' => now()->addHours(24)->toDateTimeString(),
            'user_id' => '2',
            'is_donor' => false,
            'is_receiver' => true,
            'initial_paid' => true,
            'remnant_paid' => true,
            'fully_paid' => true,
            'account_status' => AccountStatus::PAID,
            'action_button_status' => ActionButtonStatus::QUEUE,
            'created_at' => now()->subDays(7)->toDateTimeString(),
            'updated_at' => now()->subDays(7)->toDateTimeString(),
            'current_donation_date' => now()->subDays(7)->addHours(24)->toDateTimeString()
        ];

        DB::table('donations')->insert($DefaultReceiverDonation1);
        DB::table('donations')->insert($DefaultReceiverDonation2);
    }
}
