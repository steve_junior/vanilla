<?php namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
    protected $fillable = [
        'user_id', 'reason', 'comment'
    ];

    public function Identifier()
    {
        return $this->id;
    }

    public function Headline()
    {
        return $this->reason;
    }

    public function Comment()
    {
        return $this->comment;
    }

    public function Resolved()
    {
        return $this->is_resolved;
    }

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}