<?php namespace App\Models;

use App\Helpers\Enums\AccountStatus;
use App\Helpers\Enums\ActionButtonStatus;
use App\Helpers\Enums\Payment;
use App\Repository\ConfigRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


class Donation extends Model
{
    protected $table = 'donations';

    protected $fillable = [
        'deposited_amount',
        'current_amount',
        'expected_return_amount',
        'growth_rate',
        'init_amount',
        'remnant_amount',
        'queue_date',
        'user_id'
    ];

    protected $hidden = [];

    protected $dates = ['queue_date', 'current_donation_date'];

    /*
     * Boot all of the bootable traits on the model.
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function($donation) {

            $donation->current_donation_date = Carbon::now();

        });
    }

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function Identifier()
    {
        return $this->id;
    }

    public function UserId()
    {
        return $this->user_id;
    }

    public function RemnantAmount()
    {
        return $this->remnant_amount;
    }

    public function GrowthRate()
    {
        return (float) $this->growth_rate;
    }

    public function AmountCurrentGrowthPoint()
    {
       return (int) $this->current_amount;
    }

    public function PledgedAmount()
    {
        return (int) $this->deposited_amount;
    }

    public function TotalExpectedReturns()
    {
        return $this->expected_return_amount;
    }

    public function DonationWasMade()
    {
        return $this->current_donation_date;
    }

    public function DaysElapsed()
    {
        $queue_date = $this->DonationMaturityDate();

        $donation_date = $this->DonationWasMade();

        if(Carbon::now()->diffInDays($queue_date, false) >= 0)
        {
            // The queue date is still ahead of the moment
            return Carbon::now()->diffInDays($donation_date);
        }
        return $this->ConfigurationFullMaturity();
    }

    public function DonationMaturityDate()
    {
        return $this->queue_date;
    }

    public function UpdateGrowthAmounts()
    {
        $increment = $this->GrowthRate() * $this->DaysElapsed();

        $this->current_amount = (float) $this->PledgedAmount() + $increment;

        return $this->save();
    }

    public function ConfigurationFullMaturity()
    {
        $configuration = new ConfigRepository(Configuration::singleton());

        return $configuration->MaturityPeriod();
    }

    public function InitialAmount(){
        return $this->init_amount;
    }

    public function Purpose()
    {
        switch($this)
        {
            case $this->initial_paid == true and $this->remnant_paid == false:
                return Payment::REMNANT;

            case $this->initial_paid == false:
                return Payment::INITIAL;

            default:
                return Payment::ERROR;
        }
    }

    public function CanReceiveNow()
    {
       return
           $this->is_receiver == true
             and $this->fully_paid == true
                and $this->initial_paid == true
                    and $this->remnant_paid == true
                        and $this->is_donor == false
                            and $this->account_status == AccountStatus::PENDING
                                and $this->action_button_status == ActionButtonStatus::PROCESSING;
    }

    public function WaitingToDonateInitial()
    {
       return $this->is_donor == true and
                $this->initial_paid == false and
                    $this->account_status == AccountStatus::PENDING and
                        $this->action_button_status == ActionButtonStatus::NOT_YET;
    }

    /**
     * @return bool
     */
    public function WaitingToDonateRemnant()
    {
       return $this->is_donor == true  and
                 $this->is_receiver == false and
                    $this->initial_paid == true and
                        $this->remnant_paid == false and
                            $this->fully_paid == false and
                                $this->account_status == AccountStatus::PENDING and
                                    $this->action_button_status == ActionButtonStatus::NOT_YET and
                                        now()->diffInDays( $this->DonationMaturityDate(), false) > 2;


    }

}
