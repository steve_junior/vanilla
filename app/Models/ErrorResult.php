<?php namespace App\Models;

use App\Helpers\Enums\HttpStatusCode;

class ErrorResult
{
    public  $Message;
    public  $ErrorCode;

    public function _construct($message , $status_code = HttpStatusCode::NotFound)
    {
        $this->Message = $message;
        $this->ErrorCode = $status_code;
    }
}

