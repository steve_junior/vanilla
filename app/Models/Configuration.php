<?php namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    protected $fillable = [
        'factor',
        'maturity_period',
        'first_percentage',
        'remaining_percentage',
        'pre_maturity_period',
        'allow_recommitment_period',
        'minimum_donation',
        'maximum_donation'
    ];

    private static $config_instance;

    public static function singleton()
    {
        if(!isset(self::$config_instance)){

            self::$config_instance = (new Configuration())->all()->first();
        }

        return self::$config_instance;
    }

}