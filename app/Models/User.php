<?php namespace App\Models;

use App\Helpers\Enums\HttpStatusCode;
use App\Models\ApiModel\Beneficiary;
use App\Models\ApiModel\Donor;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    public $table = 'users';

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_name',
        'email',
        'password',
        'phone',
        'region',
        'payment_name',
        'payment_method',
        'payment_number',
        'referred_by',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'is_barred'    => 'boolean',
        'is_confirmed' => 'boolean'
    ];

    public function getIsBarredAttribute($value){
        return $value ? true : false;
    }

    public function donations()
    {
        return $this->hasMany(Donation::class, 'user_id');
    }

    public static function confirmed(User $user)
    {
        if($user->exists)
        {
            return $user->is_confirmed;
        }
        return new ErrorResult("User not recognised", HttpStatusCode::BadRequest);
    }

    /*
     * Boot all of the bootable traits on the model.
     */
    public static function boot()
    {
        parent::boot();

        static::creating( function($user) {

            $user->is_barred   = false;
            $user->is_confirmed = false;
            $user->last_logged_at = now();

        });
    }

    public function complaints()
    {
        return $this->hasMany(Complaint::class, 'user_id');
    }

    public function Identifier(){
        return $this->id;
    }

    public function PhoneNumber()
    {
        return $this->phone;
    }

    public function Email()
    {
       return $this->email;
    }

    public function UserName()
    {
        return $this->user_name;
    }

    public function AccountName()
    {
        return $this->payment_name;
    }

    public function AccountNumber()
    {
        return $this->payment_number;
    }

    public function AccountNetwork()
    {
        return $this->payment_method;
    }

    public function donors()
    {
        return $this->hasOne( Donor::class, 'user_id');
    }

    public function benefactors()
    {
        return $this->hasOne( Beneficiary::class, 'user_id');
    }

}
