<?php namespace App\Models\ApiModel;


use App\Models\Configuration;
use App\Models\User;
use App\Repository\UserRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Matches extends Model
{
    protected $table = 'matches';

    protected $fillable = [
        'sender_id',
        'sender_number',
        'sender_name',

        'receiver_id',
        'receiver_phone',
        'receiver_account_number',
        'receiver_name',
        'receiver_account_network',

        'is_remnant',
        'amount'
    ];

    protected $dates = ['payment_completed_by'];

    public function senderId()
    {
        return $this->sender_id;
    }

    public function Identifier()
    {
        return $this->id;
    }

    public function CompleteTransactionBy()
    {
        return $this->payment_completed_by;
    }

    public function receiverId()
    {
        return $this->receiver_id;
    }

    public function ReceiverDonation()
    {
        $member = (new UserRepository(new User()))->Find( $this->receiverId() );

        return $member->load('donations')->donations->last();
    }

    public function SenderDonation()
    {
        $member = (new UserRepository(new User()))->Find($this->senderId());

        return $member->load('donations')->donations->last();
    }

    public function receiverAccount()
    {
        return $this->receiver_name.'  '.$this->receiver_number;
    }

    public function isRemnant()
    {
        return $this->is_remnant;
    }

    public function senderAccount()
    {
        return $this->sender_name.'  '.$this->sender_number;
    }

    public static function donationPayouts($donor_id)
    {
        $payouts = self::all()->where('sender_id', $donor_id)->where('payment_confirmed', 0);

        if( $payouts->isEmpty() )
        {
            return false;            // No payouts present for the user
        }
        return $payouts->fresh();    // Payouts present for this user
    }

    public static function donationPayIns($receiver_id)
    {
        $payins = self::all()->where('receiver_id', $receiver_id)->where('payment_confirmed', 0);

        if($payins->isEmpty())
        {
            return false;            // No payins present for the user
        }
        return $payins->fresh();    // Payins present for this user
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function($match)
        {
            $match->payment_confirmed   = false;
            $match->payment_declined    = false;
            $match->has_donor_paid      = false;
            $match->payment_completed_by = Carbon::now()->addHours(Configuration::singleton()->transaction_lap_hours);
        });

        static::deleting(function($match)
        {
            if(file_exists($match->proof_of_payment))
            {
               unlink($match->proof_of_payment);
            }
        });
    }
}