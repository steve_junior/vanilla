<?php namespace App\Models\ApiModel;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;


class Beneficiary extends Model
{
    protected $table = 'beneficiaries';

    protected $fillable = [
        'donation_id',
        'user_id',
        'total_amount',
        'is_complete'
    ];

    public static function boot(){
        
        parent::boot();

        static::creating(function($beneficiary) {
            /*
             * The default amount left for beneficiary to receive is same as
             * the total amount to be received at the point of creation or initialisation
             */
            $beneficiary->amount_left =  $beneficiary->total_amount;

        });
    }

    public function UserId()
    {
        return $this->user_id;
    }

    public function DonationId()
    {
       return $this->donation_id;
    }

    public function TotalAmount()
    {
        return $this->total_amount;
    }

    public function AmountLeft()
    {
        return $this->amount_left;
    }

    public function UpdateAmountLeft($amount)
    {
       if((int) $amount == (int) 0 )
       {
           $this->is_complete = true;
       }

        $this->amount_left = (int) $amount;

        $this->save();

        return $this;
    }

    public function IsCompleted()
    {
        return $this->is_complete;
    }

    public function Id()
    {
        return $this->id;
    }

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}