<?php namespace App\Models\ApiModel;


use Illuminate\Database\Eloquent\Model;

class Sacrifice extends Model
{
    protected $table = 'sacrifices';

    protected $fillable = [
        'donation_id',
        'user_id',
        'total_amount'
    ];

    public function DonationId()
    {
        return $this->donation_id;
    }

    public function UserId()
    {
        return $this->user_id;
    }

    public function TotalAmount()
    {
        return $this->total_amount;
    }

    public function IsPartial()
    {
        return $this->is_part;
    }

}