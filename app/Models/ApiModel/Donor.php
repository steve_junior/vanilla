<?php namespace App\Models\ApiModel;


use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Donor extends Model
{
    protected $table = 'donors';

    protected $fillable = [
        'donation_id',
        'user_id',
        'total_amount',
        'is_remnant'
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function($donor){
            /*
             * The default amount left to be paid is same as
             * the total amount to be paid at the point of creation or initialisation
             */
            $donor->amount_left = $donor->total_amount;

        });
    }

    public function Id()
    {
        return $this->id;
    }

    public function DonationId()
    {
        return $this->donation_id;
    }

    public function UserId()
    {
       return $this->user_id;
    }

    public function TotalAmount()
    {
        return $this->total_amount;
    }

    public function AmountLeft()
    {
       return $this->amount_left;
    }

    public function UpdateAmountLeft( $amount )
    {
        $this->amount_left = (int) $amount;

        $this->save();

        return $this;
    }

    public function IsRemnant()
    {
        return (bool) $this->is_remnant;
    }

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}