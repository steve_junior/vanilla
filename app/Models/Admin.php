<?php namespace App\Models;


use Illuminate\Foundation\Auth\User as Authenticatable;


class Admin extends Authenticatable
{
    protected $keyType = 'string';

    public $incrementing = false;

    protected $fillable = [
        'user_name',
        'password',
        'email',
        'phone',
        'full_name',
        'account_network',
        'account_number'
    ];

   protected $guard = 'admin';

    protected $dates = ['last_logged_in'];

    /**
     * Boot all of the bootable traits on the model.
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function($admin) {
            $admin->id   = str_random(10);
        });
    }

    public function UserName()
    {
        return $this->user_name;
    }

    public function Phone()
    {
        return $this->phone;
    }

    public function AccountNumber()
    {
        return $this->account_number;
    }

    public function AccountNetwork()
    {
        return $this->account_network;
    }

    public function AccountName()
    {
        return $this->full_name;
    }
}
