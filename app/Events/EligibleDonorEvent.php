<?php namespace App\Events;

use App\Models\Donation;
use Illuminate\Queue\SerializesModels;


class EligibleDonorEvent
{
    use SerializesModels;

    public $donation;

    /*
     * Create a new event instance.
     *
     * This event is mainly called for the second stage of payment ( 60% ).
     *
     * In order to implement the at-least-2-days waiting period after the initial payment,
     * this event will be used as a mask to call all donations that conforms to stage-3 types
     * and push them into the donor's list
     *
     * @param Donation $donation
     */
    public function __construct(Donation $donation)
    {
        $this->donation = $donation;
    }
}