<?php namespace App\Events;

use App\Models\Donation;
use Illuminate\Queue\SerializesModels;


class DonationPledgedEvent
{
    use SerializesModels;

    public $donation;

    public $type;

    /**
     * Create a new event instance.
     *
     * @param Donation $donation
     * @param $type
     */
    public function __construct(Donation $donation, $type)
    {
        $this->donation = $donation;

        $this->type = (int)$type;
    }

}
