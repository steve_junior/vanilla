<?php namespace App\Events;

use App\Services\Matching\Aggregator;
use Illuminate\Queue\SerializesModels;

class InitiateBotEvent
{
    use SerializesModels;

    public $aggregator;

    /**
     * Create a new event instance.
     *
     * @param Aggregator $aggregator
     */
    public function __construct(Aggregator $aggregator)
    {
        $this->aggregator = $aggregator;
    }

}
