<?php namespace App\Events;


use App\Models\Donation;
use Illuminate\Queue\SerializesModels;


class EligibleBenefactorEvent
{
    use SerializesModels;

    public $donation;

    /**
     * Create a new event instance.
     *
     * @param Donation $donation
     */
    public function __construct(Donation $donation)
    {
        $this->donation = $donation;
    }
}