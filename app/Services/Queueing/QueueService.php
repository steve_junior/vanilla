<?php namespace App\Services\Queueing;


use App\Events\EligibleBenefactorEvent;
use App\Helpers\State;
use App\Models\Donation;

class QueueService
{
    public $donation;

    public function __construct(Donation $donation)
    {
        $this->donation = $donation;
    }

    public function Execute()
    {
        $Stage10Donation = State::Stage10( $this->donation );

        event( new EligibleBenefactorEvent( $Stage10Donation ));

        return $this;
    }
}