<?php namespace App\Services\Notification;

class FlashMessage
{
    private static  $Messages = [
        'user_not_found' => 'Sorry! member could be found for this operation',
        'user_confirmed' => 'Member was successfully confirmed',
        'user_unblocked' => 'Member was successfully unblocked',
        'user_removed' =>   'Member was successfully removed from data store'
    ];

    public static function set($key, $value)
    {
        $Keys = collect(['success', 'processing', 'error']);

        if($Keys->contains($key)){

            session()->flash($key, self::$Messages[$value]);
        }

        return;
    }
}