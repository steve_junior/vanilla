<?php namespace App\Services\Referral;

use App\Models\Donation;
use App\Repository\UserRepository;
use Illuminate\Database\Eloquent\Collection;

class ReferralService
{
    protected $repository;

    protected $authIdentifier;

    protected $collection;

    private static $PERCENTAGE = 0.05;


    public function __construct(UserRepository $userRepository, $authIdentifier)
    {
        $this->repository = $userRepository;

        $this->authIdentifier = $authIdentifier;

        $this->collection = new Collection();
    }
    
    public function RetrieveReferredMembers() : Collection
    {
        $count = 0;

        foreach ($this->repository->everyMember()->where('referred_by', $this->authIdentifier )->load('donations') as $data)
        {
            if( is_null( $lastDonation = $data->donations->last() ))
            {
                continue;
            }

            $this->collection->add(array(
                'index'         => ++$count,
                'account'       => $data->UserName(),
                'email'         => $data->Email(),
                'last_donation' => $lastDonation->PledgedAmount(),
                'share'         => $this->ComputeShare( $lastDonation )
                ));
        };

        return $this->collection;
    }
    
    protected function ComputeShare(Donation $donation)
    {
       return (int) (self::$PERCENTAGE * $donation->PledgedAmount());
    }
    
    public function ComputeSummation()
    {
        return (int) $this->collection->sum('share');
    }

    public function Link()
    {
        return route('register').'/'.$this->repository->Find( $this->authIdentifier )->UserName();
    }
}