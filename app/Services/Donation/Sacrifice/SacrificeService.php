<?php namespace App\Services\Donation\Sacrifice;

use App\Helpers\State;
use App\Models\Admin;
use App\Models\ApiModel\Sacrifice;
use App\Models\Donation;
use App\Models\User;
use App\Repository\TransactionRepository;
use App\Repository\UserRepository;



class SacrificeService
{
    protected $donation;

    protected $admin;

    protected $members;

    public function __construct(Donation $donation)
    {
        $this->donation = $donation;

        $this->admin = Admin::all()->random();

        $this->members = new UserRepository(new User() );
    }

    public function CreateMatching()
    {
        $member = $this->members->Find( $this->donation->UserId() );

        TransactionRepository::Create([
            'sender_id'     => $member->Identifier(),
            'receiver_id'   => $this->admin->id,
            'amount'        => $this->donation->InitialAmount(),
            'sender_number' => $member->PhoneNumber(),
            'sender_name'   => $member->UserName(),
            'is_remnant'    => false,

            'receiver_phone'  => $this->admin->Phone(),
            'receiver_account_number' => $this->admin->AccountNumber(),
            'receiver_account_network' => $this->admin->AccountNetwork(),
            'receiver_name' => $this->admin->AccountName(),
        ]);

        return State::Stage1( $this->donation );

    }

    public function CreateSacrifice()
    {
        Sacrifice::create([
            'donation_id'  => $this->donation->Identifier(),
            'user_id'      => $this->donation->UserId(),
            'total_amount' => $this->donation->InitialAmount()
        ]);

        return $this;
    }
}