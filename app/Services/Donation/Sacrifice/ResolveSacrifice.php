<?php namespace App\Services\Donation\Sacrifice;

use App\Events\EligibleDonorEvent;
use App\Helpers\State;
use App\Models\ApiModel\Sacrifice;
use App\Repository\DonationRepository;


class ResolveSacrifice
{
    protected $sacrifice;

    protected $donations;

    public function __construct(Sacrifice $sacrifice, DonationRepository $donationRepository)
    {
        $this->donations = $donationRepository;

        $this->sacrifice = $sacrifice;
    }

    public function Execute()
    {
        $donation = $this->donations->Find( $this->sacrifice->DonationId() );

        $Stage3Donation = State::Stage3( State::Stage2( $donation ) );

        // Pushes the donation unto the donor's list.
        event( new EligibleDonorEvent( $Stage3Donation ));

        return $this->sacrifice;
    }
}