<?php namespace App\Services\Donation;


use App\Helpers\Enums\Payment;
use App\Models\Donation;
use App\Repository\PairingRepository\DonorRepository;


class DonorService
{
    /**
     * @param Donation $donation
     * @return mixed
     * @throws \Exception
     */
    public static function Create(Donation $donation)
    {
        switch( $donation->Purpose() )
        {
            case Payment::REMNANT:
                // Donor is paying for remnant
                $data = array(
                    'donation_id'   => $donation->Identifier(),
                    'user_id'       => $donation->UserId(),
                    'total_amount'  => $donation->RemnantAmount(),
                    'is_remnant'    => true
                );
                return DonorRepository::Create( $data );

            case Payment::INITIAL:
                // Donor is paying for recommitment for new donation
                $data = array(
                    'donation_id'   => $donation->Identifier(),
                    'user_id'       => $donation->UserId(),
                    'total_amount'  => $donation->InitialAmount(),
                    'is_remnant'    => false
                );
                return DonorRepository::Create( $data );

            case Payment::ERROR:
            default:
                throw new \Exception;
        }
    }
}