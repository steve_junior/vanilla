<?php namespace App\Services\Donation;

use App\Events\DonationPledgedEvent;
use App\Helpers\Enums\Eligibility;
use App\Models\Donation;
use App\Repository\ConfigRepository;
use App\Repository\DonationRepository;
use Illuminate\Database\Eloquent\Collection;


class DonationService
{
    protected $config;

    protected $donations;

    protected $most_recent_donation;


    public function __construct(Collection $collection, ConfigRepository $configRepository)
    {
        $this->donations = $collection;

        $this->config    = $configRepository;
    }

    /**
     * @return mixed
     */
    public function getMostRecentDonation()
    {
        return $this->most_recent_donation;
    }

    /**
     * @param Donation $donation
     * @return DonationService
     */
    public function setMostRecentDonation(Donation $donation)
    {
        $this->most_recent_donation = $donation;

        return $this;
    }

    public function Eligibility($amount)
    {
        if($this->donations->isEmpty())
        {
            return Eligibility::NOVICE;    // First timer
        }

        if($amount > $this->config->Maximum() or $amount < $this->config->Minimum())
        {
            return Eligibility::DEFAULT;
        }

        $this->setMostRecentDonation( $this->donations->last() );

        if( $this->getMostRecentDonation()->deposited_amount > $amount)
        {
            return Eligibility::UNBALANCE;
        }

        if( $this->CheckRecommitmentTime( $this->getMostRecentDonation()->queue_date, $this->config) )
        {
            return Eligibility::RECOMMITMENT;
        }

        return Eligibility::DENY;
    }


    protected function CheckRecommitmentTime($queue_time, ConfigRepository $configRepository)
    {
        if( now()->diffInHours($queue_time, false) <= $configRepository->AllowedRecommitmentPeriod() )
        {
            return true;
        }
        return false;
    }


    public static function ParseAmount($amount)
    {
       // best place to avoid the member donating lesser than he donated before.
       return (int) $amount;
    }

    public static function TriggerDonationEvent(Donation $CreatedDonation, $type)
    {
        event( new DonationPledgedEvent($CreatedDonation->refresh(), $type));
        return;
    }


    public function UpdateDonationsParameters(DonationRepository $donationRepository)
    {
        foreach ($this->donations as $item)
        {
            $model = $donationRepository->Find( $item->id );

            $model->UpdateGrowthAmounts();
       }
    }

}
