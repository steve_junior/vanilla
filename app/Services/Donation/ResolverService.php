<?php namespace App\Services\Donation;

use App\Events\EligibleDonorEvent;
use App\Helpers\State;
use App\Repository\DonationRepository;
use App\Repository\TransactionRepository;


class ResolverService
{
    protected $donations;

    protected $paymentService;

    protected $transactions;


    public function __construct(DonationRepository $donationRepository, PaymentService $paymentService)
    {
        $this->donations    = $donationRepository;

        $this->paymentService      = $paymentService;

        $this->transactions = new TransactionRepository( $paymentService->modelInstance );
    }


    public function ResolveReceiver()
    {
        if($this->transactions->IsReceiverLastTransaction( $this->paymentService->modelInstance->receiverId() ))
        {
            $receiverDonation = $this->paymentService->modelInstance->ReceiverDonation();

            State::Stage12( $receiverDonation );
        }

        return $this;

    }

    public function ResolveSender()
    {
        if( $this->transactions->IsSenderLastTransaction( $this->paymentService->modelInstance->senderId() ))
        {
            $senderDonation = $this->paymentService->modelInstance->SenderDonation();


            if( $this->paymentService->modelInstance->isRemnant() )
            {
                // First check if the donation has been completely paid off before moving it to stage 5
                State::Stage6( State::Stage5( $senderDonation ) );
            }

            else {
                // this is a recommitment,
                $Stage3Donation = State::Stage3( State::Stage2( $senderDonation ) );

                // Pushes the donation unto the donor's list.
                event( new EligibleDonorEvent( $Stage3Donation ));

                // retrieve the donation prior to this one,
                $donationPriorToRecommitment = $this->donations->DonationPriorToCurrentDonation( $this->paymentService->modelInstance->senderId() );

                if( $donationPriorToRecommitment != null )
                {
                    // Trying to retrieve the previous donation of adam account which doesn't exists yet.
                    State::Stage9( State::Stage8( $donationPriorToRecommitment) );
                }

            }
        }

        return $this;
    }


}