<?php namespace App\Services\Donation;

use App\Models\ApiModel\Matches;


class PaymentService
{
    public $modelInstance;

    public function __construct(Matches $model)
    {
        $this->modelInstance = $model;
    }

    public function DonorClicksOnPaid()
    {
        $this->modelInstance->has_donor_paid = true;
        $this->modelInstance->save();

        return $this->modelInstance;
    }

    public function ReceiverConfirmsPayment()
    {
        $this->modelInstance->payment_confirmed = true;
        $this->modelInstance->payment_declined = false;
        $this->modelInstance->save();

        return $this->modelInstance;
    }

    public function ReceiverDeclinesPayment()
    {
        $this->modelInstance->payment_confirmed = false;
        $this->modelInstance->payment_declined  = true;
        $this->modelInstance->save();

        return $this->modelInstance;
    }

    public function TransactionProofProvided($uploadedFileName)
    {
        $this->modelInstance->payment_declined = true;
        $this->modelInstance->payment_confirmed = false;
        $this->modelInstance->has_donor_paid    = true;
        $this->modelInstance->proof_of_payment  = $uploadedFileName;
        $this->modelInstance->save();

        return $this->modelInstance;
    }

}