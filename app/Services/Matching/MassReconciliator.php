<?php namespace App\Services\Matching;

use App\Helpers\State;
use App\Models\ApiModel\Donor;
use App\Models\Donation;
use App\Repository\DonationRepository;
use App\Repository\TransactionRepository;
use App\Services\Matching\Interfaces\IModelState;


class MassReconciliator implements IModelState
{
    protected $secondary;

    protected $donations;

    /**
     * @param \App\Services\Matching\Ancillary $ancillary
     */
    public function __construct(Ancillary $ancillary)
    {
        $this->secondary = $ancillary;

        $this->donations = new DonationRepository( new Donation() );
    }

    public function UpdateModelStates()
    {
        foreach ( $this->secondary->GetAuxiliaryDonors() as $donor)
        {
            $donorDonation = $this->donations->Find( $donor->DonationId() );

            if( $donor->IsRemnant() )
                State::Stage4( $donorDonation );
            else State::Stage0( $donorDonation );
        }

        $benefactorDonation = $this->donations->Find( $this->secondary->GetBenefactor()->DonationId() );
        State::Stage11( $benefactorDonation );

        return $this;
    }

    protected function UpdatePartiesAmountLeft(Donor $donor)
    {
        $donorAmount    = (int) $donor->AmountLeft();
        $receiverAmount = (int) $this->secondary->GetBenefactor()->AmountLeft();

        $donor->UpdateAmountLeft( 0 );
        $benefactor = $this->secondary->GetBenefactor()->UpdateAmountLeft( $receiverAmount - $donorAmount );

        return $benefactor;
    }


    /*
     * Caution : The order of sequence in this function is crucial and should not be tampered with
     */
    public function SecondaryLinkage()
    {
        if($this->secondary->GetOverFlowStatus())
        {
            $lastContributor = $this->secondary->GetAuxiliaryDonors()->pop();
        }

        foreach ($this->secondary->GetAuxiliaryDonors() as $donor)
        {
            $pair = $this->secondary->GenerateAuxiliaryMatchingPairs( $donor );
            $benefactor = $this->UpdatePartiesAmountLeft( $donor );
            TransactionRepository::Create($pair);
            $this->secondary->UpdateBenefactor( $benefactor );
        }

        if($this->secondary->GetOverFlowStatus())
        {
            $cutoff = (int) $lastContributor->AmountLeft() - (int) $this->secondary->GetBenefactor()->AmountLeft();
            $match = $this->secondary->GenerateAuxiliaryMatchingPairs($lastContributor, true);
            $this->secondary->GetBenefactor()->UpdateAmountLeft( 0 );
            $lastContributor->UpdateAmountLeft( $cutoff );
            TransactionRepository::Create( $match );
            // $this->secondary->GetAuxiliaryDonors()->push( $lastContributor );
        }

        return $this;
    }

    /**
     * @param Cleaners $cleaner
     */
    public function SecondaryCleaner(Cleaners $cleaner)
    {
        // first remove the primary donor
        $cleaner->ExcludeDonor( $this->secondary->GetPrimaryDonor() );

        foreach($this->secondary->GetAuxiliaryDonors() as $donor)
        {
            $cleaner->ExcludeDonor( $donor );
        }
        $cleaner->ExcludeBenefactor( $this->secondary->GetBenefactor() );
    }
}