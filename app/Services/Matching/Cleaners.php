<?php namespace App\Services\Matching;

use App\Models\ApiModel\Beneficiary;
use App\Models\ApiModel\Donor;
use App\Repository\PairingRepository\BenefactorRepository;
use App\Repository\PairingRepository\DonorRepository;


class Cleaners
{
    protected $donors;

    protected $benefactors;

    public function __construct()
    {
        $this->donors = new DonorRepository( new Donor() );

        $this->benefactors = new BenefactorRepository( new Beneficiary() );
    }

    public function ExcludeBenefactor(Beneficiary $beneficiary)
    {
        $this->benefactors->Find( $beneficiary->Id() )->delete();

        return $this;
    }

    public function ExcludeDonor(Donor $donor)
    {
        $this->donors->Find( $donor->Id() )->delete();

        return $this;
    }

}