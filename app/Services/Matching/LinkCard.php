<?php namespace App\Services\Matching;

use App\Models\ApiModel\Beneficiary;
use App\Models\ApiModel\Donor;
use App\Repository\TransactionRepository;


class LinkCard extends LinkAnts
{
    protected $primaryLinkage;

    protected $primaryDonorAmountContributed;

    protected $BenefactorRemainingAmount;

    public function __construct(Beneficiary $beneficiary, Donor $donor)
    {
       parent::__construct( $beneficiary, $donor );
    }

    protected function GenerateDefaultMatchingPair()
    {
        $contributor = $this->members->Find( $this->donor->UserId() );
        $recipient   = $this->members->Find( $this->benefactor->UserId() );

        $this->primaryLinkage = array(
            'sender_number'   => $contributor->PhoneNumber(),
            'sender_name'     => $contributor->UserName(),
            'sender_id'       => $contributor->Identifier(),
            'receiver_id'     => $recipient->Identifier(),
            'receiver_phone'  => $recipient->PhoneNumber(),
            'receiver_account_number' => $recipient->AccountNumber(),
            'receiver_account_network' => $recipient->AccountNetwork(),
            'receiver_name'   => $recipient->AccountName(),
            'is_remnant'      => $this->donor->IsRemnant(),
            'amount'          => $this->donor->AmountLeft()
        );

        $this->SetPrimaryDonorContribution( $this->donor->AmountLeft() );

        return $this;
    }

    public function Linker()
    {
        $this->GenerateDefaultMatchingPair();

        $BenefactorAmountLeftToComplete = $this->GetBenefactor()->AmountLeft() - $this->GetPrimaryDonorContribution();

        $this->SetBenefactorRemainingAmount( $BenefactorAmountLeftToComplete );

        return $this;
    }

    public function Execute()
    {
        $this->benefactor = $this->benefactor->UpdateAmountLeft( (int) $this->benefactor->AmountLeft() - (int) $this->donor->AmountLeft() );

        $this->donor = $this->donor->UpdateAmountLeft(0);

        TransactionRepository::Create( $this->primaryLinkage );
    }

    public function Members()
    {
        return $this->members;
    }

    public function GetPrimaryLinkage()
    {
        return $this->primaryLinkage;
    }

    /**
     * @return mixed
     */
    public function GetPrimaryDonorContribution()
    {
        return (int) $this->primaryDonorAmountContributed;
    }

    /**
     * @param $Amount
     * @return LinkCard
     * @internal param mixed $primaryDonorAmountContributed
     */
    public function SetPrimaryDonorContribution($Amount)
    {
        $this->primaryDonorAmountContributed = $Amount;

        return $this;
    }

    /**
     * @return mixed
     */
    public function GetBenefactorRemainingAmount()
    {
        return (int) $this->BenefactorRemainingAmount;
    }

    /**
     * @param $Amount
     * @return LinkCard
     * @internal param mixed $BenefactorRemainingAmount
     */
    public function SetBenefactorRemainingAmount($Amount)
    {
        $this->BenefactorRemainingAmount = $Amount;

        return $this;
    }

}