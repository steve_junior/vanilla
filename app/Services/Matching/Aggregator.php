<?php namespace App\Services\Matching;

use App\Helpers\Enums\PairingStatus;
use App\Repository\PairingRepository\BenefactorRepository;
use App\Repository\PairingRepository\DonorRepository;


class Aggregator
{
    /*
     *  Hold all members in the donors table
     */
    protected $donors;

    /*
     *  Hold all members in the beneficiaries table
     */
    protected $benefactors;


    public function __construct(DonorRepository $donorRepository, BenefactorRepository $benefactorRepository)
    {
        $this->donors      =  $donorRepository;

        $this->benefactors =  $benefactorRepository;
    }

    public function handle()
    {
        if(! $this->CanMatchingProceed() )
        {
            logger("Apparently there's no one to give out or receive in.");

            return Condition::FAILURE;
        }

        $mapper = new LinkAnts( $this->benefactors->Pop(), $this->donors->Pop() );

        if( $mapper->Compare()->GetType() == PairingStatus::EQUAL )
        {
            $reconciliation = new Reconciliator( $mapper->Linker() );
            $reconciliation->UpdateDonorAmountLeft();
            $reconciliation->UpdateBenefactorAmountLeft();
            $reconciliation->UpdateModelStates();

            $cleaner = new Cleaners();
            $cleaner->ExcludeBenefactor( $mapper->GetBenefactor() );
            $cleaner->ExcludeDonor( $mapper->GetDonor() );

            return Condition::SUCCESSFUL;
        }

        if( $mapper->Compare()->GetType() == PairingStatus::B_UNDER_D )
        {
            /*
             * The donor has more than the receive can take...
             * We do the matching...amount to be paid will now be the recipient total_amount.
             * We update the states of both receiver and donor
             * We remove the receiver from the beneficiary list
             * Update the attributes of the donor(amount_left) without exclusion from the donors list
             */
            $reconciliation = new Reconciliator( $mapper->Linker() );
            $reconciliation->UpdateModelStates();
            $reconciliation->UpdateDonorAmountLeft();
            $reconciliation->UpdateBenefactorAmountLeft();

            (new Cleaners())->ExcludeBenefactor( $mapper->GetBenefactor() );

            return Condition::SUCCESSFUL;
        }

        if( $mapper->Compare()->GetType() == PairingStatus::B_OVER_D )
        {
            $LinkCard = (new LinkCard( $mapper->GetBenefactor(), $mapper->GetDonor() ))->Linker();

            $auxiliaries = new Ancillary( $LinkCard );

            do
            {
                // First check if there are enough donors in the system
                if( $this->AnyDonorLeft() )
                    $auxiliaries->Add( $this->donors->Pop() )->CheckSum();
                else {
                    $auxiliaries->SetType( PairingStatus::ERROR );
                    break;
                }

            } while( $auxiliaries->GetType() == PairingStatus::B_OVER_D );

            if( $auxiliaries->GetType() == PairingStatus::ERROR )
            {
                return Condition::PARTIAL;
            }

            $LinkCard->Execute();
            $massReconciliation = (new MassReconciliator( $auxiliaries ) )->UpdateModelStates()->SecondaryLinkage();

            // Apply cleaners...
            $massReconciliation->SecondaryCleaner( new Cleaners() );

            return Condition::SUCCESSFUL;
        }

        return Condition::UNEXPECTED;
    }

    protected function CanMatchingProceed()
    {
        if( $this->donors->Count() == 0 or $this->benefactors->Count() == 0)
        {
            logger("Apparently there's no one to give out or receive in.");
            return false;
        }
        return true;
    }

    protected function AnyDonorLeft()
    {
        if( $this->donors->Count() > 0 )
        {
            return true;
        }
        return false;
    }
}

abstract class Condition
    {
        const SUCCESSFUL = 1;

        const PARTIAL   = 2;

        const FAILURE   = 3;

        const UNEXPECTED = 4;
    }