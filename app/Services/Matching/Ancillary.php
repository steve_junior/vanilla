<?php namespace App\Services\Matching;

use App\Helpers\Enums\PairingStatus;
use App\Models\ApiModel\Beneficiary;
use App\Models\ApiModel\Donor;
use Illuminate\Database\Eloquent\Collection;


class Ancillary
{
    protected $ancillaryDonors;

    private $Type;

    private $OverFlow;

    private $AmountAccumulation;

    public function __construct(LinkCard $linkCard)
    {
        $this->LinkCard = $linkCard;

        $this->ancillaryDonors = new Collection();
    }

    public function Add(Donor $donor)
    {
        $this->ancillaryDonors->add( $donor );

        return $this;
    }

    public function  CheckSum()
    {
        $Sum = $this->ancillaryDonors->sum('amount_left');

        switch($this)
        {
            case $Sum == $this->LinkCard->GetBenefactorRemainingAmount():
                $this->Type     = PairingStatus::EQUAL;
                $this->OverFlow = false;
                break;

            case $Sum > $this->LinkCard->GetBenefactorRemainingAmount():
                $this->Type     = PairingStatus::B_UNDER_D;
                $this->OverFlow = true;
                break;

            case $Sum < $this->LinkCard->GetBenefactorRemainingAmount():
                $this->Type = PairingStatus::B_OVER_D;
                break;

            default: $this->Type = PairingStatus::ERROR;
                break;
        }

        $this->AmountAccumulation = $Sum;

        return $this;
    }

    public function GenerateAuxiliaryMatchingPairs(Donor $donor, $overflow = false)
    {
        $contributor = $this->LinkCard->Members()->Find( $donor->UserId() );
        $recipient   = $this->LinkCard->Members()->Find( $this->GetBenefactor()->UserId() );

        if( $overflow )
        {
            return array(
                'sender_id'       => $contributor->Identifier(),
                'sender_number'   => $contributor->PhoneNumber(),
                'sender_name'     => $contributor->UserName(),
                'receiver_id'     => $recipient->Identifier(),
                'receiver_phone'  => $recipient->PhoneNumber(),
                'receiver_account_number' => $recipient->AccountNumber(),
                'receiver_account_network' => $recipient->AccountNetwork(),
                'receiver_name'   => $recipient->AccountName(),
                'is_remnant'      => $donor->IsRemnant(),
                'amount'          => $this->GetBenefactor()->AmountLeft()
            );
        }

        return array(
            'sender_id'       => $contributor->Identifier(),
            'sender_number'   => $contributor->PhoneNumber(),
            'sender_name'     => $contributor->UserName(),
            'receiver_id'     => $recipient->Identifier(),
            'receiver_phone'  => $recipient->PhoneNumber(),
            'receiver_account_number' => $recipient->AccountNumber(),
            'receiver_account_network' => $recipient->AccountNetwork(),
            'receiver_name'   => $recipient->AccountName(),
            'is_remnant'      => $donor->IsRemnant(),
            'amount'          => $donor->AmountLeft()
        );
    }

    public function GetType()
    {
        return $this->Type;
    }

    public function SetType( $type )
    {
        $this->Type = $type;

        return $this;
    }

    public function GetAmountAccumulated()
    {
        return $this->AmountAccumulation;
    }

    public function GetAuxiliaryDonors()
    {
        return $this->ancillaryDonors;
    }
    
    public function GetBenefactor()
    {
        return $this->LinkCard->GetBenefactor();
    }

    public function GetPrimaryDonor()
    {
        return $this->LinkCard->GetDonor();
    }

    public function UpdateBenefactor(Beneficiary $beneficiary)
    {
        $this->LinkCard->UpdateBenefactor($beneficiary);

        return $this;
    }

    public function SetOverFlow($status)
    {
        $this->OverFlow = $status;

        return $this;
    }

    public function GetOverFlowStatus()
    {
        return (bool) $this->OverFlow;
    }

}