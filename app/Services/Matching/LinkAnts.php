<?php namespace App\Services\Matching;

use App\Helpers\Enums\PairingStatus;
use App\Models\ApiModel\Beneficiary;
use App\Models\ApiModel\Donor;
use App\Models\User;
use App\Repository\TransactionRepository;
use App\Repository\UserRepository;


class LinkAnts
{
    protected $benefactor;

    protected $donor;

    protected $members;

    protected $Type;

    public function __construct(Beneficiary $beneficiary, Donor $donor)
    {
        $this->benefactor = $beneficiary;

        $this->donor = $donor;

        $this->members = new UserRepository( new User() );
    }

    public function Linker()
    {
        $pair = $this->GenerateMatchingPairs();

        if(empty($pair))
        {
            throw new  \Exception;
        }
        TransactionRepository::Create($pair);

        return $this;
    }

    public function Compare()
    {
        switch($this)
        {
            case $this->benefactor->AmountLeft() == $this->donor->AmountLeft():
                $this->SetType( PairingStatus::EQUAL );
                return $this;

            case $this->benefactor->AmountLeft() < $this->donor->AmountLeft():
                $this->SetType( PairingStatus::B_UNDER_D );
                return $this;

            case $this->benefactor->AmountLeft() > $this->donor->AmountLeft():
                $this->SetType( PairingStatus::B_OVER_D );
                return $this;

            default:
                $this->SetType( PairingStatus::ERROR );
                return $this;
        }
    }

    protected function SetType($status)
    {
        switch($status)
        {
            case PairingStatus::EQUAL:
                $this->Type = PairingStatus::EQUAL;
                break;

            case PairingStatus::B_UNDER_D:
                $this->Type = PairingStatus::B_UNDER_D;
                break;

            case PairingStatus::B_OVER_D:
                $this->Type = PairingStatus::B_OVER_D;
                break;

            default: $this->Type = PairingStatus::ERROR;
        }
    }

    public function GetType()
    {
       return $this->Type;
    }

    protected function GenerateMatchingPairs()
    {
        $contributor = $this->members->Find( $this->donor->UserId() );
        $recipient   = $this->members->Find( $this->benefactor->UserId() );

        $pair = array();

        switch($this->Type)
        {
            case PairingStatus::EQUAL:
                $data = [
                    'sender_id'       => $contributor->Identifier(),
                    'sender_number'   => $contributor->PhoneNumber(),
                    'sender_name'     => $contributor->UserName(),
                    'receiver_id'     => $recipient->Identifier(),
                    'receiver_phone'  => $recipient->PhoneNumber(),
                    'receiver_account_number' => $recipient->AccountNumber(),
                    'receiver_account_network' => $recipient->AccountNetwork(),
                    'receiver_name'   => $recipient->AccountName(),
                    'is_remnant'      => $this->donor->IsRemnant(),
                    'amount'          => $this->donor->TotalAmount()
                ];
                break;

            case PairingStatus::B_UNDER_D:
                $data = [
                    'sender_id'       => $contributor->Identifier(),
                    'sender_number'   => $contributor->PhoneNumber(),
                    'sender_name'     => $contributor->UserName(),
                    'receiver_id'     => $recipient->Identifier(),
                    'receiver_phone'  => $recipient->PhoneNumber(),
                    'receiver_account_number' => $recipient->AccountNumber(),
                    'receiver_account_network' => $recipient->AccountNetwork(),
                    'receiver_name'   => $recipient->AccountName(),
                    'is_remnant'      => $this->donor->IsRemnant(),
                    'amount'          => $this->benefactor->TotalAmount()
                ];
                break;

            case PairingStatus::B_OVER_D:
            case PairingStatus::ERROR:
            default:
                $data = [];
            break;
        }

        return array_merge($pair, $data);
    }

    public function GetBenefactor()
    {
        return $this->benefactor;
    }

    public function UpdateBenefactor(Beneficiary $beneficiary)
    {
        $this->benefactor = $beneficiary;

        return $this;
    }

    public function GetDonor()
    {
        return $this->donor;
    }
}