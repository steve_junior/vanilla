<?php namespace App\Services\Matching;


use App\Helpers\State;
use App\Models\Donation;
use App\Repository\DonationRepository;
use App\Services\Matching\Interfaces\IModelState;

class Reconciliator implements IModelState
{
    protected $pair;

    public function __construct(LinkAnts $linker)
    {
        $this->pair     =  $linker;

        $this->donations = new DonationRepository( new Donation() );
    }

    public function UpdateModelStates()
    {
        $donorDonation    = $this->donations->Find( $this->pair->GetDonor()->DonationId() );

        $receiverDonation = $this->donations->Find( $this->pair->GetBenefactor()->DonationId() );

        if($this->pair->GetDonor()->IsRemnant())
            State::Stage4( $donorDonation );
        else State::Stage0( $donorDonation );

        State::Stage11( $receiverDonation );

        return $this;
    }

    public function UpdateDonorAmountLeft()
    {
        $donorAmount    = (int) $this->pair->GetDonor()->AmountLeft();
        $receiverAmount = (int) $this->pair->GetBenefactor()->AmountLeft();

        $model = $this->pair->GetDonor()->UpdateAmountLeft( $donorAmount - $receiverAmount );

        return $model;
    }

    public function UpdateBenefactorAmountLeft()
    {
        $donorAmount    = (int) $this->pair->GetDonor()->AmountLeft();
        $receiverAmount = (int) $this->pair->GetBenefactor()->AmountLeft();

        $model = $this->pair->GetBenefactor()->UpdateAmountLeft( $receiverAmount - $donorAmount );

        return $model;
    }
}