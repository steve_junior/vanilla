<?php namespace App\Services\Support;


use App\Models\Complaint;
use App\Repository\SupportRepository;
use Illuminate\Database\Eloquent\Collection;

class ResolveComplaintService
{
    public function __construct(SupportRepository $supportRepository)
    {
        $this->supportRepository = $supportRepository;

        $this->collection = new Collection();
    }

    public function RetrieveComplaints()
    {
        foreach ($this->supportRepository->EveryComplaint() as $complaint)
        {
           $user = self::AttachUserModel( $complaint )->users;

            $this->collection->add([
                'id'         => $complaint->Identifier(),
                'created_at' => $complaint->created_at->toDayDateTimeString(),
                'subject'    => $complaint->Headline(),
                'comment'    => $complaint->Comment(),
                'user_name'  => $user->UserName(),
                'contact'    => $user->PhoneNumber(),
                'account_name' => $user->AccountName(),
                'user_id'    => $user->Identifier(),
                'status'     => $complaint->Resolved()
            ]);
        }
        return $this->collection;
    }

    private static function AttachUserModel(Complaint $complaint)
    {
        return $complaint->load('users');
    }

    public function ChangeComplaintStatus($id, $markAsResolved = true)
    {
        $complaint = $this->supportRepository->Find( $id);

        $complaint->is_resolved = $markAsResolved;

        return $complaint->save();
    }
}