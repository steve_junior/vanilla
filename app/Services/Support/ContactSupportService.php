<?php namespace App\Services\Support;

use App\Repository\ComplaintRepository;

class ContactSupportService
{
    static $MAX_COMPLAINT = 3;

    public function __construct(ComplaintRepository $complaintRepository)
    {
        $this->ComplaintsByUser = $complaintRepository;
    }

    public function AllowComplaint()
    {
        if($this->ComplaintsByUser->unresolved()->count() >= self::$MAX_COMPLAINT)
        {
            return false;
        }
        return true;
    }
}