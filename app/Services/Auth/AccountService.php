<?php namespace App\Services\Auth;

use App\Http\Controllers\Auth\LoginController;
use App\Models\User;


class AccountService
{
    public static function Block(User $member)
    {
        $member->is_barred = true;

        $member->save();

        return $member;
    }
}