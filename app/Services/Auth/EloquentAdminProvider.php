<?php namespace App\Services\Auth;


use App\Models\Admin;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Support\Str;


class EloquentAdminProvider extends  EloquentUserProvider
{
    public function __construct(Hasher $hasher, $adminModel)
    {
        if(strcmp($adminModel, 'App\Models\Admin') == 0)
        {
            parent::__construct($hasher, $adminModel);
        }

        else abort(403, "Model Instance not of Admin");
    }
}