<?php namespace App\Helpers;

use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;

class ImageProcessing
{
    protected $file;

    public function __construct(UploadedFile $uploadedFile)
    {
        $this->file = $uploadedFile;
    }

    public function Save($match_id)
    {
        if(isset($match_id)){

            if(! file_exists(public_path(Directories::PROOF_OF_PAYMENT)))
            {
                mkdir(public_path(Directories::PROOF_OF_PAYMENT), 0777, true);
            }
            
            $fileName = Directories::PROOF_OF_PAYMENT.time().'_'.$match_id.'.'.$this->file->getClientOriginalExtension();

            Image::make($this->file)->resize(800, 800)->save(public_path($fileName));

            return $fileName;
        }

        throw new \Exception;
    }

}