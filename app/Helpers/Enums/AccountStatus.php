<?php namespace App\Helpers\Enums;


abstract class AccountStatus {

    const MERGED = "Merged";

    const PENDING = "Pending";

    const PAID = "Paid";

    const COMPLETED = "Completed";

    const ALL = array(
        'paid'       => self::PAID,
        'merged'     => self::MERGED,
        'pending'    => self::PENDING,
        'completed'  => self::COMPLETED
    );
}


abstract class ActionButtonStatus
{
    const RECOMMIT = "Recommit";

    const QUEUE = "Add To Queue";

    const NOT_YET = "Not Yet";

    const PROCESSING = "Processing";

    const ALL = array(
        'recommit'       => self::RECOMMIT,
        'queue'          => self::QUEUE,
        'not_yet'        => self::NOT_YET,
        'processing'     => self::PROCESSING
    );
}

abstract class Payment
{
    const INITIAL = 1;

    const REMNANT = 2;

    const ERROR  = 3;
}
