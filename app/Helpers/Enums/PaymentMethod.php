<?php namespace App\Helpers\Enums;

abstract class PaymentMethod
{
    const MTN = "MTN Mobile Money";

    const GLO = "Glo Mobile Money";

    const VODAFONE = "Vodafone Cash";

    const TIGO = "Tigo Cash";

    const AIRTEL = "Airtel Money";

    const ERROR = "Error";

    const METHODS = array(
        'mtn'    => self::MTN,
        'tigo'   => self::TIGO,
        'voda'   => self::VODAFONE,
        'glo'    => self::GLO,
        'airtel' => self::AIRTEL
    );

    public static function GetPaymentNetwork($attribute)
    {
        switch($attribute)
        {
            case 'mtn':
                return self::MTN;

            case 'tigo':
                return self::TIGO;

            case 'voda':
                return self::VODAFONE;

            case 'glo':
                return self::GLO;

            case 'airtel':
                return self::AIRTEL;

            default:
                return self::ERROR;
        }
    }
}