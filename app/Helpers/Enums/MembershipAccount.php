<?php namespace App\Helpers\Enums;


abstract class MembershipAccount
{
    const BARRED = 1;

    const UNCONFIRMED = 2;

    const ALLOWED = 3;

    const DENY = 4;
}