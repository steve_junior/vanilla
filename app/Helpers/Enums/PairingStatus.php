<?php namespace App\Helpers\Enums;


abstract class PairingStatus
{
    const EQUAL = 0;

    const B_OVER_D = 11;

    const B_UNDER_D = 10;

    const ERROR = 500;
}