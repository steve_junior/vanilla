<?php namespace App\Helpers\Enums;


abstract class DonationRange
{
    const MAX = 2000;

    const MIN = 100;
}

abstract class DonationType
{
    const SACRIFICE = 100;
    const STANDARD  = 200;
}