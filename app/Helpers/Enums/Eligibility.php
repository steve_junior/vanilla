<?php namespace App\Helpers\Enums;


abstract class Eligibility
{
    const NOVICE = 1;

    const RECOMMITMENT = 2;

    const DENY = 3;

    const UNBALANCE = 4;

    const DEFAULT = 5;
}