<?php namespace App\Helpers;

use App\Helpers\Enums\AccountStatus;
use App\Helpers\Enums\ActionButtonStatus;
use App\Models\Donation;


class State
{
    public static function Stage0(Donation $donation)
    {
        $donation->is_donor             = true;
        $donation->is_receiver          = false;
        $donation->initial_paid         = false;
        $donation->remnant_paid         = false;
        $donation->fully_paid           = false;
        $donation->account_status       = AccountStatus::PENDING;
        $donation->action_button_status = ActionButtonStatus::NOT_YET;

        $donation->save();
        return $donation;
    }

    public static function Stage1(Donation $donation)
    {
        $donation->is_donor             = true;
        $donation->is_receiver          = false;
        $donation->initial_paid         = false;
        $donation->remnant_paid         = false;
        $donation->fully_paid           = false;
        $donation->account_status       = AccountStatus::MERGED;
        $donation->action_button_status = ActionButtonStatus::NOT_YET;

        $donation->save();
        return $donation;
    }

    public static function Stage2(Donation $donation)
    {
        $donation->is_donor             = true;
        $donation->is_receiver          = false;
        $donation->initial_paid         = true;
        $donation->remnant_paid         = false;
        $donation->fully_paid           = false;
        $donation->account_status       = AccountStatus::PAID;
        $donation->action_button_status = ActionButtonStatus::NOT_YET;

        $donation->save();
       return $donation;
    }

    public static function Stage3(Donation $donation)
    {
        $donation->is_donor             = true;
        $donation->is_receiver          = false;
        $donation->initial_paid         = true;
        $donation->remnant_paid         = false;
        $donation->fully_paid           = false;
        $donation->account_status       = AccountStatus::PENDING;
        $donation->action_button_status = ActionButtonStatus::NOT_YET;

        $donation->save();
        return $donation;
    }

    public static function Stage4(Donation $donation)
    {
        $donation->is_donor             = true;
        $donation->is_receiver          = false;
        $donation->initial_paid         = true;
        $donation->remnant_paid         = false;
        $donation->fully_paid           = false;
        $donation->account_status       = AccountStatus::MERGED;
        $donation->action_button_status = ActionButtonStatus::NOT_YET;

        $donation->save();
        return $donation;
    }

    public static function Stage5(Donation $donation)
    {
        $donation->is_donor             = true;
        $donation->is_receiver          = false;
        $donation->initial_paid         = true;
        $donation->remnant_paid         = true;
        $donation->fully_paid           = false;
        $donation->account_status       = AccountStatus::PAID;
        $donation->action_button_status = ActionButtonStatus::NOT_YET;

        $donation->save();
        return $donation;
    }

    public static function Stage6(Donation $donation)
    {
        $donation->is_donor             = true;
        $donation->is_receiver          = false;
        $donation->initial_paid         = true;
        $donation->remnant_paid         = true;
        $donation->fully_paid           = false;
        $donation->account_status       = AccountStatus::PENDING;
        $donation->action_button_status = ActionButtonStatus::RECOMMIT;

        $donation->save();
        return $donation;
    }

    public static function Stage7(Donation $donation)
    {
        $donation->is_donor             = true;
        $donation->is_receiver          = false;
        $donation->initial_paid         = true;
        $donation->remnant_paid         = true;
        $donation->fully_paid           = false;
        $donation->account_status       = AccountStatus::PENDING;
        $donation->action_button_status = ActionButtonStatus::NOT_YET;

        $donation->save();
        return $donation;
    }

    public static function Stage8(Donation $donation)
    {
        $donation->is_donor             = true;
        $donation->is_receiver          = false;
        $donation->initial_paid         = true;
        $donation->remnant_paid         = true;
        $donation->fully_paid           = false;
        $donation->account_status       = AccountStatus::MERGED;
        $donation->action_button_status = ActionButtonStatus::NOT_YET;

        $donation->save();
        return $donation;
    }

    public static function Stage9(Donation $donation)
    {
        $donation->is_donor             = false;
        $donation->is_receiver          = true;
        $donation->initial_paid         = true;
        $donation->remnant_paid         = true;
        $donation->fully_paid           = true;
        $donation->account_status       = AccountStatus::PAID;
        $donation->action_button_status = ActionButtonStatus::QUEUE;

        $donation->save();
        return $donation;
    }

    public static function Stage10(Donation $donation)
    {
        $donation->is_donor             = false;
        $donation->is_receiver          = true;
        $donation->initial_paid         = true;
        $donation->remnant_paid         = true;
        $donation->fully_paid           = true;
        $donation->account_status       = AccountStatus::PENDING;
        $donation->action_button_status = ActionButtonStatus::PROCESSING;

        $donation->save();
        return $donation;
    }

    public static function Stage11(Donation $donation)
    {
        $donation->is_donor             = false;
        $donation->is_receiver          = true;
        $donation->initial_paid         = true;
        $donation->remnant_paid         = true;
        $donation->fully_paid           = true;
        $donation->account_status       = AccountStatus::MERGED;
        $donation->action_button_status = ActionButtonStatus::NOT_YET;

        $donation->save();
        return $donation;
    }

    public static function Stage12(Donation $donation)
    {
        $donation->is_donor             = false;
        $donation->is_receiver          = true;
        $donation->initial_paid         = true;
        $donation->remnant_paid         = true;
        $donation->fully_paid           = true;
        $donation->account_status       = AccountStatus::COMPLETED;
        $donation->action_button_status = ActionButtonStatus::NOT_YET;

        $donation->save();
        return $donation;
    }
}