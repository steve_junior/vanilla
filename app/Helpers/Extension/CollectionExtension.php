<?php namespace App\Helpers\Extension;


use Illuminate\Database\Eloquent\Collection;

class CollectionExtension extends Collection
{
    public function append(...$items)
    {
        foreach ($items as $item)
        {
            $this->items = array_prepend($this->items, $item);
        }
        return $this;
    }
}