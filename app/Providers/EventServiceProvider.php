<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [

        'App\Events\EligibleDonorEvent' => [
            'App\Listeners\EligibleDonorListener'
        ],

        'App\Events\EligibleBenefactorEvent' => [
            'App\Listeners\EligibleBenefactorListener'
        ],

        'App\Events\DonationPledgedEvent' => [
            'App\Listeners\DonationPledgedListener'
        ],

        'App\Events\InitiateBotEvent' => [
            'App\Listeners\InitiateBotListener'
        ],


    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
