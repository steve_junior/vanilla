<?php namespace App\Providers;


use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider  extends ServiceProvider
{
    /**
    * Register bindings in the container.
    *
    * @return void
    */
    public function boot()
    {
        // Using class based composers...
        View::composer(
            'layout.internal-main', 'App\Http\ViewComposers\MemberLayoutComposer'
        );
        View::composer(
            'app.dashboard' , 'App\Http\ViewComposers\MemberDashboardComposer'
        );
        View::composer(
            'layout.admin-layout' , 'App\Http\ViewComposers\AdminLayoutComposer'
        );
        View::composer(
            'admin.dashboard' , 'App\Http\ViewComposers\AdminPersonalDashboardComposer'
        );

        View::composer(
            'admin.systems-dashboard' , 'App\Http\ViewComposers\AdminSystemDashboardComposer'
        );

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}