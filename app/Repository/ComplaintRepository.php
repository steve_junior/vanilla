<?php namespace App\Repository;


use App\Models\Complaint;
use App\Repository\Interfaces\ISearch;

class ComplaintRepository implements ISearch
{
    protected $complaints;

    public function __construct(Complaint $complaint)
    {
        $this->complaints = $complaint->all();
    }

    public function unresolved()
    {
        return $this->complaints->where('is_resolved', false);
    }

    public function resolved()
    {
        return $this->complaints->where('is_resolved', true);
    }

    public function recentWeek($no_of_week)
    {
        $past_times = now()->subWeeks((int) $no_of_week)->toDateTimeString();

        return $this->complaints->where('created_at', '>=', $past_times);
    }

    public function complaintByUserId($id)
    {
       $this->complaints = $this->complaints->where('user_id', $id);

       return $this;
    }

    public function resolve($id)
    {
       $complaint = $this->Find($id);

       $complaint->is_resolved = true;

       $complaint->save();

       $this->complaints = $this->complaints->fresh();

        return $this;
    }

    public function unresolve($id)
    {
        $complaint = $this->Find($id);

        $complaint->is_resolved = false;

        $complaint->save();

        $this->complaints = $this->complaints->fresh();

        return $this;
    }

    public function Find($id)
    {
        return $this->complaints->fresh()->find($id);
    }
}