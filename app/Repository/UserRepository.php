<?php namespace App\Repository;

use App\Models\User;
use App\Repository\Interfaces\ISearch;


/**
 * @property \Illuminate\Database\Eloquent\Collection|static[] users
 */
class UserRepository implements ISearch
{
    protected $users;

    public function __construct(User $user)
    {
        $this->users = $user->all();
    }

    public function barredMembers()
    {
        return $this->users->where('is_barred', true);
    }

    public function activeMembers()
    {
       return $this->users->where('is_barred', false)->where('is_confirmed', true);
    }

    public function Count()
    {
        return $this->users->count();
    }

    public function unconfirmedAccounts()
    {
       $this->users = $this->users->where('is_confirmed', false);

        return $this->users;
    }

    public function weeklyParticipants()
    {
        $past7days = now()->subWeek()->toDateTimeString();

        return  $this->users->where('created_at', '>=', $past7days);
    }

    public function everyMember()
    {
        return $this->users;
    }

    public function deleteMember($id)
    {
        return $this->users;
    }

    public function Find($id)
    {
        return $this->users->fresh()->find( $id );
    }

    public function FindByUserName( $user_name )
    {
        return $this->users->where('user_name', $user_name)->first();
    }

    public function Exists($member_id)
    {
        return $this->users->contains('id', $member_id);
    }

}