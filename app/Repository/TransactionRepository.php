<?php namespace App\Repository;


use App\Models\ApiModel\Matches;
use App\Repository\Interfaces\ISearch;


class TransactionRepository implements ISearch
{
    public function __construct(Matches $pairs)
    {
        $this->records = $pairs->all();
    }


    public function unresolvedConflictTransactions()
    {
        return $this->records->where('payment_declined', true)->where('payment_confirmed', false)->where('has_donor_paid', true);
    }

    public function unconfirmedPayments()
    {
        $this->records = $this->records->where('payment_confirmed', false);

        return $this;
    }

    public function Find($id)
    {
        return $this->records->fresh()->find($id);
    }

    public static function Create(array $data)
    {
        return Matches::create( $data );
    }

    public function IsSenderLastTransaction($sender_id)
    {
        if( $this->records->where('sender_id', $sender_id)->count() > (int) 1 )
        {
            return false;
        }
        return true;
    }

    public function IsReceiverLastTransaction($receiver_id)
    {
        if( $this->records->where('receiver_id', $receiver_id)->count() > (int) 1 )
        {
            return false;
        }
        return true;
    }
}