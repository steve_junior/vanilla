<?php namespace App\Repository;


use App\Helpers\State;
use App\Models\Donation;
use App\Repository\Interfaces\ISearch;

class DonationRepository implements ISearch
{
    protected $donations;

    public function __construct(Donation $donation)
    {
        $this->donations = $donation->all();
    }

    public function donationsByUserId($id)
    {
        $this->donations = $this->donations->where('user_id', $id);

        return $this->donations;
    }

    public function Find($id)
    {
        return $this->donations->fresh()->find($id);
    }

    public static function Create(array $data)
    {
        $donation =  Donation::create($data);

        // Every donation must start the cycle from stage 0.
        return State::Stage0($donation);
    }

    public function EveryDonation()
    {
        return $this->donations;
    }

    public function Contains($id)
    {
        return $this->donations->contains('id', $id);
    }

    public function DonationPriorToCurrentDonation( $user_id )
    {
        $donations = $this->donations->where('user_id', $user_id);

        $donations->pop();

        return $donations->pop();
    }

    public function LastDonation()
    {
        return $this->donations->last();
    }

    public function Stage3Donations()
    {
        $donations = $this->donations->reject( function($donation)
        {
             return !$donation->WaitingToDonateRemnant();
        });

        return $donations;
    }

}