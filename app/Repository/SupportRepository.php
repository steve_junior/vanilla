<?php namespace App\Repository;


use App\Models\Complaint;
use App\Repository\Interfaces\ISearch;

class SupportRepository implements ISearch
{
    public $complaints;

    public function __construct(Complaint $complaint)
    {
        $this->complaints = $complaint->all();
    }

    public function EveryComplaint()
    {
        return $this->complaints;
    }


    public function Find($id)
    {
        return $this->complaints->fresh()->find($id);
    }

    public function UnresolvedIssues()
    {
        return $this->complaints->where('is_resolved', false)->sortByDesc('created_at');
    }
}