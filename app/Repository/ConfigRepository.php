<?php namespace App\Repository;


use App\Models\Configuration;

class ConfigRepository
{
    protected $config;

    public function __construct(Configuration $configuration)
    {
        $this->config = $configuration->singleton();
    }

    public function PrematurityPeriod()
    {
        return $this->config->pre_maturity_period;
    }

    public function MaturityPeriod()
    {
        return $this->config->maturity_period;
    }

    public function FirstPercentage()
    {
        return $this->config->first_percentage;
    }

    public function RemainingPercentage()
    {
        return $this->config->remaining_percentage;
    }

    public function Minimum()
    {
        return (int) $this->config->minimum_donation;
    }

    public function Maximum()
    {
        return (int) $this->config->maximum_donation;
    }

    public function Factor()
    {
        return $this->config->factor;
    }

    public function AllowedRecommitmentPeriod()
    {
        return $this->config->allow_recommitment_period;
    }

    public function DecimalPlaces()
    {
        return $this->config->amount_decimal_places;
    }
}