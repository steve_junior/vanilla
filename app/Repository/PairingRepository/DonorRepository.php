<?php namespace App\Repository\PairingRepository;


use App\Models\ApiModel\Donor;
use App\Repository\Interfaces\ISearch;


class DonorRepository implements ISearch
{
    public $donors;

    public function __construct(Donor $donor)
    {
        $this->donors = $donor->all();
    }

    public function Exists($donation_id, $user_id = null)
    {
        if (! is_null($user_id)) {
            return $this->donors->contains('user_id', $user_id);
        }
        return $this->donors->contains('donation_id', $donation_id);
    }

    public static function Create(array $data)
    {
        return Donor::create($data);
    }

    public function Count()
    {
        return $this->donors->count();
    }

    public function Pop() : Donor
    {
        return $this->donors->pop();
    }

    public function First()
    {
        return $this->donors->first();
    }

    public function Find($id)
    {
        return $this->donors->fresh()->find($id);
    }

    public function EveryMember()
    {
        return $this->donors;
    }
}