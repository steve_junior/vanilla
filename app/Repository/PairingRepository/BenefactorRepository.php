<?php namespace App\Repository\PairingRepository;


use App\Models\ApiModel\Beneficiary;

class BenefactorRepository
{
    public $benefactors;

    public function __construct(Beneficiary $beneficiary)
    {
        $this->benefactors = $beneficiary->all();
    }

    public function Exists($donation_id, $user_id = null)
    {
        if(! is_null($user_id))
        {
            return $this->benefactors->contains('user_id', $user_id);
        }

        return $this->benefactors->contains('donation_id', $donation_id);
    }

    public static function Create(array $data)
    {
        $benefactor = Beneficiary::create($data);

        return $benefactor;
    }

    public function Count()
    {
        return $this->benefactors->count();
    }

    public function Find($id)
    {
        return $this->benefactors->fresh()->find( $id );
    }

    public function EveryMember()
    {
        return $this->benefactors;
    }

    public function Pop()
    {
        return $this->benefactors->pop();
    }
}