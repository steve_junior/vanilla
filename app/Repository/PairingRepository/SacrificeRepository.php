<?php namespace App\Repository\PairingRepository;

use App\Models\ApiModel\Sacrifice;
use App\Repository\Interfaces\ISearch;

class SacrificeRepository implements ISearch
{
    protected $records;

    public $sacrifice;

    public function __construct(Sacrifice $sacrifice)
    {
        $this->records = $sacrifice->all();
    }

    public function Find($id)
    {
        return $this->records->fresh()->find($id);
    }

    public function FindByDonationId($donation_id) : Sacrifice
    {
        return $this->records->where('donation_id', $donation_id)->first();
    }

    public function FindByUserId($user_id) : Sacrifice
    {
        $record = $this->records->where('user_id', $user_id)->first();

        return $this->Find($record->id);
    }
}