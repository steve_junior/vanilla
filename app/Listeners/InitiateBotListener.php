<?php namespace App\Listeners;

use App\Events\InitiateBotEvent;
use App\Services\Matching\Condition;

class InitiateBotListener
{
    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  InitiateBotEvent  $event
     * @return void
     */
    public function handle(InitiateBotEvent $event)
    {
        $response = $event->aggregator->handle();

        switch( $response )
        {
            case Condition::SUCCESSFUL:
                session()->flash('success', 'Matching Completed');
                break;

            case Condition::PARTIAL:
                session()->flash('error', 'Matching could not be completed');
                break;

            case Condition::FAILURE:
                session()->flash('error', 'Matching Failed');
                break;

            case Condition::UNEXPECTED:
                session()->flash('error', 'Something unexpected happened during matching');
                break;

            default:
                session()->flash('error', 'Default condition reached');
        }

    }
}
