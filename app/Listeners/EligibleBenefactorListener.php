<?php namespace App\Listeners;

use App\Events\EligibleBenefactorEvent;
use App\Repository\PairingRepository\BenefactorRepository;

class EligibleBenefactorListener
{
    /**
     * Create the event listener
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param EligibleBenefactorEvent $event
     */
    public function handle(EligibleBenefactorEvent $event)
    {
        if($event->donation->CanReceiveNow() == false)
        {
            return;
        }

        else {
            $data = array(
                'donation_id'  => $event->donation->Identifier(),
                'user_id'      => $event->donation->UserId(),
                'total_amount' => $event->donation->TotalExpectedReturns(),
                'is_complete'  => false
            );

            BenefactorRepository::Create($data);
        }
    }

}