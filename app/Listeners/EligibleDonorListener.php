<?php namespace App\Listeners;

use App\Events\EligibleDonorEvent;
use App\Repository\DonationRepository;
use App\Repository\PairingRepository\DonorRepository;
use App\Services\Donation\DonorService;


class EligibleDonorListener
{
    protected $donorRepository;
    /**
     * Create the event listener
     * @param DonorRepository $donorRepository
     */
    public function __construct(DonorRepository $donorRepository)
    {
        $this->donorRepository = $donorRepository;
    }

    /**
     * Handle the event.
     *
     * In order to implement the at-least-2-days waiting period after the initial payment,
     * this event will be used as a mask to call all donations that conforms to stage-3 types
     *  and are older than 2 days and push them into the donor's list
     *
     * @param  EligibleDonorEvent $event
     * @throws \Exception
     */
    public function handle(EligibleDonorEvent $event)
    {
        $donationRepository = new DonationRepository( $event->donation );

        foreach ($donationRepository->Stage3Donations() as $donation)
        {
           if( $this->donorRepository->Exists( $donation->Identifier() ))
           {
                continue;
           }
           else DonorService::Create( $donation );
        }

    }

}