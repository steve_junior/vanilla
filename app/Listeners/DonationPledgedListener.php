<?php namespace App\Listeners;

use App\Events\DonationPledgedEvent;
use App\Helpers\Enums\DonationType;
use App\Helpers\State;
use App\Repository\DonationRepository;
use App\Services\Donation\DonorService;
use App\Services\Donation\Sacrifice\SacrificeService;
use Illuminate\Support\Facades\Auth;


class DonationPledgedListener
{
    /**
     * Create the event listener
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param DonationPledgedEvent $event
     * @throws \Exception
     */
    public function handle(DonationPledgedEvent $event)
    {
        if($event->type == DonationType::STANDARD)
        {
            // Update the state of the previous donation before creating the new donation
            $DonationPriorToLastDonation = (new DonationRepository($event->donation))->DonationPriorToCurrentDonation( Auth::user()->getAuthIdentifier() );

            State::Stage7( $DonationPriorToLastDonation );

            DonorService::Create( $event->donation );
        }

        elseif($event->type == DonationType::SACRIFICE)
        {
            (new SacrificeService($event->donation))->CreateSacrifice()->CreateMatching();
        }

        else return;
    }

}