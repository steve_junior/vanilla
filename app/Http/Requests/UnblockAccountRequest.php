<?php namespace App\Http\Requests;

use App\Helpers\Enums\Admin;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UnblockAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guard(Admin::GUARD)->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'member_id' => 'required | exists:users,id',
            'email' => 'required | email | exists:users,email',
            'user_name' => 'required | exists:users,user_name'
        ];
    }
}
