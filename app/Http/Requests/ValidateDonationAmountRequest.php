<?php namespace App\Http\Requests;

use App\Helpers\Enums\DonationRange;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ValidateDonationAmountRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'donation_amount' => 'required|integer|min:'.DonationRange::MIN.'|max:'.DonationRange::MAX
        ];
    }


    /**
     * Get the proper failed validation response for the request.
     *
     * @param  array $errors
     * @return JsonResponse
     */
    public function response(array $errors)
    {
        return view('app.donate', $errors);
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'donation_amount.min' => 'Minimum donation is '.DonationRange::MIN.' cedis.',
            'donation_amount.max' => 'Maximum donation is '.DonationRange::MAX.' cedis.'
        ];
    }
}
