<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_name'     => 'regex:/^\S*$/|max:255|unique:users,user_name',  // prevent spaces in the  username because of referral link
            'email'         => 'required|email|unique:users,email|max:255',
            'password'      => 'required|confirmed|regex:/^[A-Za-z0-9@!#\$%\^&\*]+$/',
            'phone'         => 'required|min:10',
            'region'        => 'sometimes|max:255',
            'payment_name'  => 'required|max: 255',
            'payment_method'=> 'required|max: 255',
            'payment_number'=> 'required|max: 255',
            'guide'          => 'required',
            'terms'          => 'accepted'
        ];
    }

    /**
     * Get the proper failed validation response for the request.
     *
     * @param  array $errors
     * @return JsonResponse
     */
    public function response(array $errors)
    {
        return view('guest.register', $errors);
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => 'We need to know your e-mail address!',
            'user_name.regex' => 'Spaces are not allowed in user name'
        ];
    }
}
