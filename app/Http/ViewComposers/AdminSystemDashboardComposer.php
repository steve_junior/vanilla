<?php namespace App\Http\ViewComposers;


use App\Repository\PairingRepository\BenefactorRepository;
use App\Repository\PairingRepository\DonorRepository;
use App\Repository\UserRepository;
use Illuminate\View\View;

class AdminSystemDashboardComposer
{
    protected $active_members;
    protected $barred_members;
    protected $donors;
    protected $benefactors;
    protected $weekly_members;


    /**
     * Create a new profile composer.
     * @param UserRepository $userRepository
     * @param DonorRepository $donorRepository
     * @param BenefactorRepository $benefactorRepository
     */
    public function __construct(UserRepository $userRepository, DonorRepository $donorRepository, BenefactorRepository $benefactorRepository)
    {
        // Dependencies automatically resolved by service container...
        $this->active_members = $userRepository->activeMembers()->Count();
        $this->barred_members = $userRepository->barredMembers()->Count();
        $this->weekly_members = $userRepository->weeklyParticipants()->Count();
        $this->donors        = $donorRepository->Count();
        $this->benefactors   = $benefactorRepository->Count();
    }

    /**
     * Bind data to the view.

     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with([
            'active_members'  => $this->active_members,
            'barred_members'  => $this->barred_members,
            'weekly_members'  => $this->weekly_members,
            'donors'          => $this->donors,
            'benefactors'     => $this->benefactors
        ]);
    }

}