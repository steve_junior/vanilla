<?php namespace App\Http\ViewComposers;

use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class MemberLayoutComposer
{
    /**
     * The user repository implementation.
     *
     * @var Auth
     */
    protected $user;

    /**
     * Create a new profile composer.
     *
     * @param  Auth  $user
     * @return void
     */
    public function __construct()
    {
        // Dependencies automatically resolved by service container...
        $this->user = Auth::user();
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with([
            'name' => $this->user->user_name,
            'ip'   => request()->ip(),
        ]);
    }
}