<?php namespace App\Http\ViewComposers;

use App\Helpers\Enums\AccountStatus;
use App\Helpers\Enums\ActionButtonStatus;
use App\Models\ApiModel\Matches;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class MemberDashboardComposer
{
    protected $payins;

    protected $payouts;


    /**
     * Create a new profile composer.
     *
     * @internal param Auth $user
     * @param Matches $matches
     */
    public function __construct()
    {
        // Dependencies automatically resolved by service container...
        $user = Auth::user();

        $this->payouts = Matches::donationPayouts($user->getAuthIdentifier());

        $this->payins = Matches::donationPayIns($user->getAuthIdentifier());
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with([
            'PayIns'                => $this->payins,
            'PayOuts'               => $this->payouts,
            'AccountStatus'         => AccountStatus::ALL,
            'ActionButtonStatus'    => ActionButtonStatus::ALL,
        ]);
    }
}