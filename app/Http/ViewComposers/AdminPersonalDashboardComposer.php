<?php namespace App\Http\ViewComposers;


use App\Helpers\Enums\Admin;
use App\Models\ApiModel\Matches;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class AdminPersonalDashboardComposer
{
    /**
     * The user repository implementation.
     *
     * @var Auth
     */
    protected $admin;

    protected $payins;

    /**
     * Create a new profile composer.
     *
     * @internal param Auth $user
     * @param Matches $matches
     */
    public function __construct()
    {
        // Dependencies automatically resolved by service container...
        $this->admin = Auth::guard(Admin::GUARD)->user();

        $this->payins = Matches::donationPayIns( $this->admin->getAuthIdentifier() );

    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with([
            'PayIns'  => $this->payins,
        ]);
    }
}