<?php

namespace App\Http\ViewComposers;


use App\Helpers\Enums\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class AdminLayoutComposer
{
    protected $admin;


    public function __construct()
    {
        $this->admin = Auth::guard(Admin::GUARD)->user();
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with([
            'name' => $this->admin->user_name,
            'ip'   => request()->ip()
        ]);
    }
}