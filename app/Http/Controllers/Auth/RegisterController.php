<?php namespace App\Http\Controllers\Auth;

use App\Helpers\Enums\PaymentMethod;
use App\Models\User;
use App\Http\Controllers\Controller;
use App\Repository\UserRepository;
use App\Traits\RegistersUserAccounts;
use Illuminate\Database\Eloquent\Collection;

class RegisterController extends Controller
{
    use RegistersUserAccounts;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected  $redirectTo = '/dashboard';

    /**
     * Container of all users
     *
     * @var string
     */
    private  $userRepository;


    /**
     * Create a new controller instance.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;

        $this->middleware('guest');
    }

    protected function AppendUserGuide(Collection $data)
    {
       return $this->userRepository->FindByUserName( $data->pull('guide') )->Identifier();
    }

    protected static function ConvertPaymentMethod(Collection $data)
    {
        return PaymentMethod::GetPaymentNetwork( $data->pull('payment_method') );
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $input
     * @return User
     * @internal param array $data
     */
    protected function create(array $input)
    {
        $data = $this->Converter( new Collection($input) );

        $response = array (
            'user_name'      => $data['user_name'],
            'email'          => $data['email'],
            'password'       => bcrypt( $data['password'] ),
            'phone'          => $data['phone'],
            'region'         => $data['region'],
            'payment_name'   => $data['payment_name'],
            'payment_method' => $data['payment_method'],
            'payment_number' => $data['payment_number'],
            'referred_by'    => $data['referred_by']
        );

        return User::create($response);
    }

    protected function Converter(Collection $data)
    {
        $data->put('payment_method', self::ConvertPaymentMethod($data) );

        if($data->get('guide') == 'admin' or $data->get('guide') == 'Admin')
        {
            $data->put('referred_by', null);
        }
        else $data->put('referred_by', self::AppendUserGuide($data));

        return $data->toArray();
    }
}
