<?php namespace App\Http\Controllers\Admin;

use App\Helpers\Enums\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests\ResolveConflictRequest;
use App\Repository\DonationRepository;
use App\Repository\TransactionRepository;
use App\Repository\UserRepository;
use App\Services\Auth\AccountService;
use App\Services\Donation\PaymentService;
use App\Services\Donation\ResolverService;
use Illuminate\Support\Facades\Auth;


class ConflictResolutionController extends Controller
{
    protected $admin;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {

            $this->admin = Auth::guard(Admin::GUARD)->user();

            return $next($request);
        });
    }

    public function index(TransactionRepository $transacts)
    {
        return view('admin.conflicts.index', ['records' => $transacts->unresolvedConflictTransactions()]);
    }

    public function confirm(TransactionRepository $transacts, ResolveConflictRequest $request, DonationRepository $donationRepository)
    {
        $paymentService = new PaymentService( $transacts->Find($request->get('record_id')) );

        (new ResolverService( $donationRepository, $paymentService))->ResolveReceiver()->ResolveSender();

        $paymentService->ReceiverConfirmsPayment()->delete();

        return redirect()->route('admin-resolve');
    }

    /**
     * @param TransactionRepository $transacts
     * @param ResolveConflictRequest $request
     * @param UserRepository $userRepository
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function decline(TransactionRepository $transacts, ResolveConflictRequest $request, UserRepository $userRepository)
    {
        $pairing = $transacts->Find( $request->get('record_id'));

        $paymentService = new PaymentService( $pairing );

        $member = $userRepository->Find( $paymentService->ReceiverDeclinesPayment()->senderId() );

        AccountService::Block( $member );

        if($pairing->receiverId() == $this->admin->getAuthIdentifier())
        {
            $paymentService->modelInstance->delete();
        }

        // Announce rematching here if the receiver is not an admin.

        return redirect()->route('admin-resolve');
    }
}
