<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class DashboardController extends Controller
{
    protected $admin;

    /**
     * Create a new controller instance.
     *
     * @param Auth $auth
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {

            $this->admin = Auth::guard('admin')->user();

            return $next($request);
        });
    }

    public function index()
    {
        return view('admin.dashboard');
    }

    public function advanced()
    {
        return view('admin.systems-dashboard');
    }
}