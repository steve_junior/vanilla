<?php namespace App\Http\Controllers\Admin;


use App\Helpers\Enums\Admin;
use App\Http\Controllers\Controller;
use App\Repository\DonationRepository;
use App\Repository\PairingRepository\SacrificeRepository;
use App\Repository\TransactionRepository;
use App\Services\Donation\PaymentService;
use App\Services\Donation\Sacrifice\ResolveSacrifice;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Admin\PayConfirmationRequest;
use App\Http\Requests\Admin\PayDeclinationRequest;


class PaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware(function($request, $next)
        {
            Auth::guard(Admin::GUARD)->user();

            return $next($request);
        });
    }

    /*
     * Donors payment is confirmed by the receiver..
     */
    public function ConfirmedPayment(PayConfirmationRequest $request, TransactionRepository $paymentRepository, SacrificeRepository $sacrificeRepository, DonationRepository $donationRepository)
    {
        $paymentService = new PaymentService( $paymentRepository->Find( $request->get('match_id')));

        $resolverSacrifice = new ResolveSacrifice( $sacrificeRepository->FindByUserId( $paymentService->modelInstance->senderId() ), $donationRepository);

        $resolverSacrifice->Execute()->delete();

        $paymentService->ReceiverConfirmsPayment()->delete();

        return redirect()->route('admin-dashboard');
    }


    public function DeclinedPayment(PayDeclinationRequest $request, TransactionRepository $paymentRepository)
    {
        $paymentService = new PaymentService( $paymentRepository->Find( $request->get('match_id')));

        $paymentService->ReceiverDeclinesPayment();

        return redirect()->route('admin-dashboard');
    }
}