<?php namespace App\Http\Controllers\Admin;


use App\Helpers\Enums\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateConfigsRequest;
use App\Models\Configuration;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ConfigurationController extends Controller
{
    protected $admin;

    public function __construct()
    {
        $this->middleware( function($request, $next)
        {
            $this->admin = Auth::guard(Admin::GUARD)->user();

            return $next($request);
        });
    }

    public function index()
    {
        $config = Configuration::all()->first()->toArray();

        return view('admin.configurations', ['config' => $config]);
    }

    public function update(UpdateConfigsRequest $request)
    {
        $configuration = Configuration::singleton()->first();

        $configuration->factor = $request->factor;
        $configuration->maturity_period = $request->maturity_period;
        $configuration->first_percentage = $request->first_percentage;
        $configuration->remaining_percentage = $request->remaining_percentage;
        $configuration->pre_maturity_period = $request->pre_maturity_period;
        $configuration->minimum_donation = $request->minimum_donation;
        $configuration->maximum_donation = $request->maximum_donation;
        $configuration->transaction_lap_hours = $request->transaction_lap_hours;

        $configuration->save();

        return redirect()->route('configurations');
    }
}