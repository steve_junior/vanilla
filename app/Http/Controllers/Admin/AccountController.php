<?php namespace App\Http\Controllers\Admin;


use App\Helpers\Enums\Admin;
use App\Helpers\Enums\PaymentMethod;
use App\Http\Controllers\Controller;
use App\Http\Requests\EditAdminAccountDetailsRequest;
use App\Http\Requests\EditAdminPasswordRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{
    protected $admin;

    public function __construct()
    {
        $this->middleware(function($request, $next)
        {
            $this->admin = Auth::guard(Admin::GUARD)->user();

            return $next($request);
        });
    }

    public function profile()
    {
        $profile = array(
            'full_name' => $this->admin->full_name,
            'phone'     => $this->admin->phone,
            'email'     => $this->admin->email,
            'last_logged_in' => Carbon::parse($this->admin->last_logged_at)->toDateTimeString()
        );

        return view('admin.account.profile', ['admin' => $profile]);
    }

    public function accountDetails()
    {
        $data = array(
            'payment_method' => $this->admin->payment_method,
            'payment_name'   => $this->admin->payment_name,
            'payment_number' => $this->admin->payment_number
        );

        $response = array(
            'admin'    => $data,
            'methods' => PaymentMethod::METHODS
        );

        return view('admin.account.details', $response);
    }

    public function updateAccountDetails(EditAdminAccountDetailsRequest $request)
    {
        $admin = \App\Models\Admin::find($this->admin->getAuthIdentifier());

        $admin->payment_method = $request->payment_method;
        $admin->payment_number = $request->payment_number;

        $admin->save();
        return redirect()->route('admin-update-details');
    }

    public function changePassword(EditAdminPasswordRequest $request)
    {
        if (!(Hash::check($request->get('current_password'), $this->admin->password))) {
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->get('current_password'), $request->get('new_password')) == 0){
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }

        $this->admin->password = bcrypt($request->get('new_password'));
        $this->admin->save();

        return redirect()->back()->with("success","Password changed successfully !");
    }

}