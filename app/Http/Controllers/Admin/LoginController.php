<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Traits\AuthenticatesAdminAccount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    use AuthenticatesAdminAccount;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '';

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->redirectTo  = route('admin-dashboard');

        $this->middleware('guest:admin')->except('logout');
    }

}