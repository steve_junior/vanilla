<?php namespace App\Http\Controllers\Admin;

use App\Events\InitiateBotEvent;
use App\Helpers\Enums\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\TriggerPairingRequest;
use App\Repository\PairingRepository\BenefactorRepository;
use App\Repository\PairingRepository\DonorRepository;
use App\Services\Matching\Aggregator;
use Illuminate\Support\Facades\Auth;


class MatchingController extends Controller
{
    protected $admin;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {

            $this->admin = Auth::guard(Admin::GUARD)->user();

            return $next($request);
        });
    }

    public function index(DonorRepository $donorRepository, BenefactorRepository $benefactorRepository)
    {
        return view('admin.linking', ['donors' => $donorRepository->EveryMember()->load('users'), 'benefactors' => $benefactorRepository->EveryMember() ]);
    }

    public function match(TriggerPairingRequest $request, BenefactorRepository $benefactorRepository, DonorRepository $donorRepository)
    {
        $aggregator = new Aggregator($donorRepository, $benefactorRepository);

        event( new InitiateBotEvent( $aggregator ) );

        return redirect()->route('admin-match');
    }
}