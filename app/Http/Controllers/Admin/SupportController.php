<?php namespace App\Http\Controllers\Admin;

use App\Helpers\Enums\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests\ComplaintResolveRequest;
use App\Repository\SupportRepository;
use App\Services\Support\ResolveComplaintService;
use Illuminate\Support\Facades\Auth;


class SupportController extends Controller
{
    public function __construct()
    {
        $this->middleware( function ($request, $next)
        {
            Auth::guard( Admin::GUARD )->user();

            return $next($request);
        });
    }

    public function index(SupportRepository $supportRepository)
    {
        $SupportService = new ResolveComplaintService( $supportRepository );

        return view('admin.support', [ 'records' => $SupportService->RetrieveComplaints() ]);
    }

    public function resolve(ComplaintResolveRequest $request, SupportRepository $supportRepository)
    {
        $SupportService = new ResolveComplaintService( $supportRepository );

        $SupportService->ChangeComplaintStatus( $request->get('complaint_id') );

        return redirect()->route('support');
    }

    public function unresolve(ComplaintResolveRequest $request, SupportRepository $supportRepository)
    {
        $SupportService = new ResolveComplaintService( $supportRepository );

        $SupportService->ChangeComplaintStatus( $request->get('complaint_id'), false );

        return redirect()->route('support');
    }
}