<?php namespace App\Http\Controllers\Admin;


use App\Helpers\Enums\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests\ConfirmAccountRequest;
use App\Http\Requests\DeleteAccountRequest;
use App\Http\Requests\UnblockAccountRequest;
use App\Http\Requests\UpdateMemberRequest;
use App\Models\User;
use App\Repository\UserRepository;
use App\Services\Notification\FlashMessage;
use Illuminate\Support\Facades\Auth;


class ManageMembersController extends Controller
{
    protected $admin;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {

            $this->admin = Auth::guard(Admin::GUARD)->user();

            return $next($request);
        });
    }

    public function index(UserRepository $users)
    {
        $members = $users->everyMember();

        return view('admin.members.index', ['members' => $members ]);
    }

    public function view(UserRepository $users, $member_id)
    {
        $member = $users->Find($member_id);

        if($member ==  null)
        {
            FlashMessage::set('error', 'user_not_found');
            return redirect()->back();
        }

        return view('admin.members.view', ['member' => $member->load('donations')]);
    }

    public function edit(UserRepository $repository, $member_id)
    {
        $member = $repository->Find( $member_id);

        if($member == null)
        {
            FlashMessage::set('error', 'user_not_found');
            return redirect()->back();
        }

        return view('admin.members.edit', ['member' => $member->load('donations')]);
    }

    public function update(UserRepository $userRepository, $member_id, UpdateMemberRequest $request)
    {
        if(! $userRepository->Exists( $member_id ))
        {
            FlashMessage::set('error', 'user_not_found');
            return redirect()->back();
        }

        $userRepository->Find( $member_id )->update( $request->all() );

        return redirect()->back();
    }

    public function confirm(UserRepository $users, ConfirmAccountRequest $request)
    {
        $member = $users->Find($request->get('member_id'));

        if($member == null)
        {
            FlashMessage::set('error', 'user_not_found');
            return redirect()->route('admin-members');
        }

        $member->is_confirmed = true;
        $member->save();

        FlashMessage::set('success', 'user_confirmed');
        return redirect()->route('admin-members');
    }

    public function unblock(UserRepository $users, UnblockAccountRequest $request)
    {
        $member = $users->Find($request->get('member_id'));

        if($member == null)
        {
            FlashMessage::set('error', 'user_not_found');
            return redirect()->route('admin-members');
        }

        $member->is_barred = false;
        $member->save();

        FlashMessage::set('success', 'user_unblocked');
        return redirect()->route('admin-members');
    }

    public function remove(UserRepository $users, DeleteAccountRequest $request)
    {
        $member = $users->Find($request->get('member_id'));

        if($member == null)
        {
            FlashMessage::set('error', 'user_not_found');
            return redirect()->route('admin-members');
        }

        User::destroy( $member->id );

        FlashMessage::set('success', 'user_unblocked');
        return redirect()->route('admin-members');
    }
}