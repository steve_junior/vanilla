<?php namespace App\Http\Controllers\Members;

use App\Helpers\Enums\DonationType;
use App\Helpers\Enums\Eligibility;
use App\Http\Controllers\Controller;
use App\Http\Requests\ValidateDonationAmountRequest;
use App\Repository\ConfigRepository;
use App\Repository\DonationRepository;
use App\Services\Donation\DonationService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;



class  DonationController extends Controller
{
    protected  $user;
    protected  $amount;
    protected  $config;

    public function __construct(ConfigRepository $configRepository)
    {
        $this->config = $configRepository;

        $this->middleware(function($request, $next)
        {
            $this->user = Auth::user();

            return $next($request);
        });
    }

    public function index()
    {
        return view('app.donate');
    }

    /**
     * @param ValidateDonationAmountRequest $request
     * @param DonationRepository $repository
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function donate(ValidateDonationAmountRequest $request, DonationRepository $repository)
    {
        $donationService = new DonationService($repository->donationsByUserId($this->user->getAuthIdentifier()), $this->config);

        $this->amount = $donationService::ParseAmount( $request->get('donation_amount') );

        switch ($donationService->Eligibility( $this->amount )) {

            case Eligibility::RECOMMITMENT:
                $donationService->TriggerDonationEvent($this->create(), DonationType::STANDARD);
                return redirect()->route('member-dashboard');

            case Eligibility::NOVICE:
                $donationService->TriggerDonationEvent($this->create(), DonationType::SACRIFICE);
                return redirect()->route('member-dashboard');

            case Eligibility::UNBALANCE:
                return view('app.donate')->withErrors('Your pledge should be greater than or equal to c'.$donationService->getMostRecentDonation()->deposited_amount);

            case Eligibility::DEFAULT:
                return view('app.donate')->withErrors('Your pledge must be within c'.$this->config->Minimum().' and c'.$this->config->Maximum());

            case Eligibility::DENY:
            default:
                return view('app.donate')->withErrors('Ineligible to make donations at the moment.');
        }
    }

    protected function create()
    {
        $data = array(
            'deposited_amount'      => $this->amount,
            'current_amount'        => $this->amount,
            'expected_return_amount'=> $this->expectedReturnAmount(),
            'growth_rate'           => $this->calculateGrowthRate(),
            'init_amount'           => $this->calculateInitPayment(),
            'remnant_amount'        => $this->calculateRemnantAmount(),
            'queue_date'            => Carbon::now()->addDays( $this->config->MaturityPeriod() ),
            'user_id'               => $this->user->getAuthIdentifier()
        );

      return DonationRepository::Create($data);
    }


    private function CalculateInitPayment()
    {
        return $this->amount * $this->config->FirstPercentage();
    }

    private function calculateRemnantAmount()
    {
        return $this->amount * $this->config->RemainingPercentage();
    }

    private function calculateGrowthRate()
    {
        $profit = $this->expectedReturnAmount() - $this->amount;

        $rate_per_day = (int)$profit / $this->config->MaturityPeriod();

        return round($rate_per_day, $this->config->DecimalPlaces());
    }

    private function expectedReturnAmount()
    {
        return $this->amount * $this->config->Factor();
    }

}