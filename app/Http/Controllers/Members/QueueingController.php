<?php namespace App\Http\Controllers\Members;

use App\Http\Controllers\Controller;
use App\Http\Requests\PushToQueueRequest;
use App\Repository\DonationRepository;
use App\Services\Queueing\QueueService;
use Illuminate\Support\Facades\Auth;



class QueueingController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->middleware(function($request, $next){

            $this->user = Auth::user();

            return $next($request);
        });
    }


    /**
     * @param PushToQueueRequest $request
     * @param DonationRepository $donationRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke(PushToQueueRequest $request, DonationRepository $donationRepository)
    {
        $donation = $donationRepository->Find( $request->get('donation_id') );

        (new QueueService( $donation ))->Execute();

        return redirect()->route('member-dashboard');
    }
}