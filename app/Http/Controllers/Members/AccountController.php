<?php namespace App\Http\Controllers\Members;

use App\Helpers\Enums\AccountStatus;
use App\Helpers\Enums\PaymentMethod;
use App\Http\Controllers\Controller;
use App\Http\Requests\EditAccountDetailsRequest;
use App\Http\Requests\EditPasswordRequest;
use App\Models\Donation;
use App\Models\User;
use App\Repository\DonationRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {

            $this->user = Auth::user();

            return $next($request);
        });
    }

    public function profile()
    {
        $donation = Donation::all()->where('user_id', $this->user->getAuthIdentifier());

        if($donation->isNotEmpty())
        {
            $profile = array(
                'payment_name' => $this->user->payment_name,
                'phone'        =>  $this->user->phone,
                'email'        => $this->user->email,
                'region'       => $this->user->region,
                'deposited_amount' => $donation->last()->deposited_amount,
                'last_logged_at'   => Carbon::parse($this->user->last_logged_at)->toDayDateTimeString()
            );

            return view('app.profile')->with('user', $profile);
        }

        $profile = array(
            'payment_name'    => $this->user->payment_name,
            'phone'           =>  $this->user->phone,
            'email'           => $this->user->email,
            'region'          => $this->user->region,
            'deposited_amount' => 'Not donation yet',
            'last_logged_at'   => Carbon::parse($this->user->last_logged_at)->toDayDateTimeString()
        );

        return view('app.profile')->with('user', $profile);
    }

    public function accountDetails()
    {
        $data = array(
            'payment_method' => $this->user->payment_method,
            'payment_name'   => $this->user->payment_name,
            'payment_number' => $this->user->payment_number
        );

        $response = array(
            'user'    => $data,
            'methods' => PaymentMethod::METHODS
        );

        return view('app.account', $response);
    }

    public function updateAccountDetails(EditAccountDetailsRequest $request)
    {
        if(is_null($this->user))
        {
            return redirect()->route('login');
        }

        $user = User::find($this->user->getAuthIdentifier());
        $user->payment_method = $request->payment_method;
        $user->payment_number = $request->payment_number;

        $this->user = $user->save();
        return redirect()->route('details');
    }

    public function password()
    {

    }

    public function changePassword(EditPasswordRequest $request)
    {
        if (!(Hash::check($request->get('current_password'), $this->user->password))) {
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->get('current_password'), $request->get('new_password')) == 0){
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }

        $this->user->password = bcrypt($request->get('new_password'));
        $this->user->save();

        return redirect()->back()->with("success","Password changed successfully !");
    }

    public function history(DonationRepository $donationRepository)
    {
        $donations = $donationRepository->donationsByUserId($this->user->getAuthIdentifier())
                                    ->where('account_status', AccountStatus::COMPLETED);

        $data = new Collection();

        foreach ($donations as $donation)
        {
            $data->add( array(
                'account_status' => $donation->account_status,
                'donation_date'  => Carbon::parse($donation->current_donation_date)->toDayDateTimeString(),
                'amount'         => $donation->deposited_amount
            ));
        }
        return view('app.history', ['data' => $data]);
    }
}