<?php namespace App\Http\Controllers\Members;

use App\Http\Controllers\Controller;
use App\Models\Configuration;
use App\Models\Donation;
use App\Repository\ConfigRepository;
use App\Repository\DonationRepository;
use App\Services\Donation\DonationService;
use Illuminate\Support\Facades\Auth;

class DashboardController extends  Controller
{
    protected  $user;
    protected  $donations;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {

            $this->user = Auth::user();

            return $next($request);
        });
    }

    /**
     * @param DonationRepository $repository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(DonationRepository $repository)
    {
        $this->donations = $repository->donationsByUserId( $this->user->getAuthIdentifier() );

        if($this->donations->isEmpty())
        {
            return view('app.dashboard', ['donations' => null]);
        }

        $donationService = new DonationService($this->donations, new ConfigRepository(Configuration::singleton()));

        $donationService->UpdateDonationsParameters($repository);

        return view('app.dashboard', ['donations' => $this->donations->fresh()]);
    }
}