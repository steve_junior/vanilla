<?php namespace App\Http\Controllers\Members;


use App\Http\Controllers\Controller;
use App\Repository\UserRepository;
use App\Services\Referral\ReferralService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ReferralController extends Controller
{
    protected $user;

    protected $members;

    public function __construct(UserRepository $userRepository)
    {
        $this->members = $userRepository;

        $this->middleware(function($request, $next)
        {
            $this->user = Auth::user();

            return $next($request);
        });
    }

    public function index()
    {
        $service = new ReferralService( $this->members, $this->user->getAuthIdentifier() );

        return view('app.referral.index', [ 'data' => $service->RetrieveReferredMembers() ]);
    }

    public function view()
    {
        $link = (new ReferralService($this->members, $this->user->getAuthIdentifier()) )->Link();

        return view('app.referral.view', ['referrallink' => $link]);
    }

    public function cashout(Request $request)
    {

        /* Make sure that the claim is true
         * Retrieve all his members
         * Update this referred_by columns to NULL
         * Push the user into a special queue to receive his money after 7 days.
         */
        return $request;
    }
}