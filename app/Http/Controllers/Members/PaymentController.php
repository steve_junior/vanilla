<?php namespace App\Http\Controllers\Members;

use App\Helpers\ImageProcessing;
use App\Http\Controllers\Controller;
use App\Http\Requests\DonorPaidRequest;
use App\Http\Requests\PayConfirmationRequest;
use App\Http\Requests\PayDeclinationRequest;
use App\Http\Requests\UploadProofRequest;
use App\Repository\DonationRepository;
use App\Repository\TransactionRepository;
use App\Services\Donation\PaymentService;
use App\Services\Donation\ResolverService;
use Illuminate\Support\Facades\Auth;


class PaymentController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->middleware(function($request, $next)
        {
            $this->user = Auth::user();

            return $next($request);
        });
    }

//    public function Paid(DonorPaidRequest $request, TransactionRepository $paymentRepository)
//    {
//        $record = $paymentRepository->Find( $request->get('match_id') );
//
//        $paymentService = new PaymentService( $record );
//
//        if($record->payment_confirmed)
//            $paymentService->modelInstance->delete();
//
//        else $paymentService->DonorClicksOnPaid();
//
//        return redirect()->route('member-dashboard');
//    }

    /*
     * * Check if the payment_confirmed is true
        * if payment_confirmed is false just alert the recipient of the activity without the delete the match model.
        * else good?
        * We identify the type of payment that was done, whether recommitment or 60% (remnant) payment.
        * Based on the type, we retrieve the donation,
        *   For remnant payment => update the state of the donation
        *   For recommitment payment => retrieve both current and the most recent donations for the member for
        *                           which the recommitment was made.
        *      1. Update the state of the ms donation.
        *      2. Update the state of the current donation, call event for the donation to move to the next stage.
     */

    public function Paid(DonorPaidRequest $request, TransactionRepository $paymentRepository, DonationRepository $donationRepository)
    {
        $record = $paymentRepository->Find( $request->get('match_id'));

        $paymentService = new PaymentService( $record );

        if($record->payment_confirmed)
        {
            (new ResolverService($donationRepository, $paymentService))->ResolveReceiver()->ResolveSender();

            $paymentService->ReceiverConfirmsPayment()->delete();
        }

        else $paymentService->DonorClicksOnPaid();

        return redirect()->route('member-dashboard');
    }

    /*
     * Donors payment is confirmed by the receiver..
     */
    public function ConfirmedPayment(PayConfirmationRequest $request, TransactionRepository $paymentRepository, DonationRepository $donationRepository)
    {
       $paymentService = new PaymentService( $paymentRepository->Find( $request->get('match_id')) );

        (new ResolverService($donationRepository, $paymentService))->ResolveSender()->ResolveReceiver();

       $paymentService->ReceiverConfirmsPayment()->delete();

        return redirect()->route('member-dashboard');
    }


    public function DeclinedPayment(PayDeclinationRequest $request, TransactionRepository $paymentRepository)
    {
        $paymentService = new PaymentService($paymentRepository->Find( $request->get('match_id')));

        $paymentService->ReceiverDeclinesPayment();

        return redirect()->route('member-dashboard');
    }

    /*
     * Used when the donor clicked on I have paid and the recipient declines the payment...
     * Donor is required to upload proof with 3hrs.
     * else Account is barred.
     */
    public function ProvidePaymentProof(UploadProofRequest $request, TransactionRepository $paymentRepository)
    {
        $paymentService = new PaymentService( $paymentRepository->Find( $request->get('match_id')));

        $imageProcessor = new ImageProcessing( $request->file('picture') );

        $paymentService->TransactionProofProvided( $imageProcessor->Save($paymentService->modelInstance->Identifier() ));

        return redirect()->route('member-dashboard');
    }

}