<?php namespace App\Http\Controllers\Members;


use App\Http\Controllers\Controller;
use App\Http\Requests\ContactSupportRequest;
use App\Models\Complaint;
use App\Repository\ComplaintRepository;
use App\Services\Support\ContactSupportService;
use Illuminate\Support\Facades\Auth;

class SupportController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {

            $this->user = Auth::user();

            return $next($request);
        });
    }

    public function index()
    {
        return view('app.complain');
    }

    public function complain(ContactSupportRequest $request, ComplaintRepository $repository)
    {
        $SupportService = new ContactSupportService( $repository->complaintByUserId($this->user->getAuthIdentifier()));

        if($SupportService->AllowComplaint())
        {
           $this::create($request->all());

            $success_message = "Message was successfully sent. Be patient while the support team work on your request.";
        }

        else $success_message = $success_message = "Please you have exhausted the number of complaints you can make to the admin. You cannot make further request until the past complaints have been resolved.";

        session()->flash('success', $success_message);

        return redirect()->route('contact-support');
    }

    protected function create(array $data)
    {
        return Complaint::create([
                'user_id' => $this->user->getAuthIdentifier(),
                'reason'  => $data['reason'],
                'comment' => $data['comment']
        ]);
    }
}