<?php namespace App\Traits;

use App\Helpers\Enums\PaymentMethod;
use App\Http\Requests\CreateUserRequest;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

trait RegistersUserAccounts
{
    use RedirectsUsers;

   public function showRegistrationForm($referral = null)
   {
       return view('guest.register', [ 'methods' =>  PaymentMethod::METHODS, 'source' => $referral ]);
   }

    public function register(CreateUserRequest $request)
    {
        event(new Registered($user = $this::create($request->all())));

        $this->guard()->login($user);

        if(User::confirmed($user))
        {
            return $this->registered($request, $user);
        }
        else return redirect()->intended('/login');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  User  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        if($user->exists && $request->user()->email == $user->email && $request->user()->user_name == $user->user_name)
        {
            return redirect()->intended($this->redirectPath());
        }
        return redirect()->intended('/home');
    }

}