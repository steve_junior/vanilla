<?php namespace App\Traits;

use App\Events\PaymentQueued;
use App\Helpers\Enums\AccountStatus;
use App\Helpers\Enums\ActionButtonStatus;
use Illuminate\Http\Request;


trait QueueingSupervisor
{

    /**
     * @param Request $request
     *
     * @return bool|\Illuminate\Database\Eloquent\Model|static
     */
    protected function verifyDonation(Request $request)
    {
        $donation = $this->donations->find( $request->get('donation_id') );

        if($donation->exists)
        {
            return $donation;
        }
        return false;
    }

    protected function processQueueing($donation)
    {
        event(new PaymentQueued($donation));

        $donation->account_status       = AccountStatus::PENDING;
        $donation->action_button_status = ActionButtonStatus::PROCESSING;
        $donation->save();

        $response =  $this->donations->fresh();

        return $response;
    }

}