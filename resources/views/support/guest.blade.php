@extends('layout.external-main')


@section('support-guest')

    <div class="account-wrapper">
        <div class="account-body">
            <section class="content">
                <!-- Default Form -->
                <!-- general form elements disabled -->
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Contact Support</h3>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <form method="GET" class="form account-form" role="form" action = '{{ url('/') }}'>
                            <input type='hidden' name='' value='' />
                            <!-- text input -->
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder = "Username" name="username" required>
                            </div> <!-- /.form-group -->

                            <div class="form-group">
                                <input class="form-control" type="text" placeholder = "Reason" name="reason" required>
                            </div> <!-- /.form-group -->

                            <div class="form-group">
                                <textarea class="form-control" rows="3" placeholder ="Comment" name="comment"></textarea>
                            </div> <!-- /.form-group -->

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block btn-lg" tabindex="4">
                                    Submit
                                </button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->        <!-- /.box -->
            </section>
        </div>
    </div>

@endsection