<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="">
    <meta name="description" content="GiversRoom is a financial mutual aid platform, that yearns to improve lives in ghana. ">
    <meta name="keywords" content="mutual aid funds, make money without referrals, peer to peer no referral, peer to peer, money making referrals, money investment, worldwide investment, investment that pays">
    <meta property="og:image" content = "static/images/social.html"/>
    <title>P2P | Lorem ipsum dolor sit amet, consectetur adipiscing elit.</title>

    <!-- core CSS -->
    <link href="static/css/bootstrap.min.css" rel="stylesheet">
    <link href="static/css/font-awesome.min.css" rel="stylesheet">
    <link href="static/css/animate.min.css" rel="stylesheet">
    <link href="static/css/prettyPhoto.css" rel="stylesheet">
    <link href="static/css/main.css" rel="stylesheet">
    <link href="static/css/responsive.css" rel="stylesheet">
    <link rel="shortcut icon" href="static/images/logo/ico.png">
</head>

<body class="homepage">

<section id = "subsubfooter">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <img class="img-responsive wow fadeInDown" data-wow-duration="500ms" data-wow-delay="600ms" src="static/images/security/1.png">
            </div>
        </div>
    </div>
</section>

<hr class="account-header-divider">

<div class="account-wrapper">

    <div class="account-body">
        <div class="account-logo">
            <a href="{{ url('/') }}"><img src="#" alt="P2P Logo"></a>
        </div>


        <h2 class="account-body-title">Password Reset</h2>
        <h5 class="account-body-subtitle">We'll email you instructions on how to reset your password.</h5>

        <form class="form account-form" action = 'https://www.giversroom.com/password/reset/' method = "POST"> <input type='hidden' name='csrfmiddlewaretoken' value='b92YAi6tBXsPGS6oowUAFo6UAOYa6gBY' />

            <div class="form-group">
                <label for="forgot-email" class="placeholder-hidden">Your Username</label>
                <input type="text" class="form-control" name = "username" placeholder="Username"  tabindex="1">
            </div> <!-- /.form-group -->

            <div class="form-group">
                <label for="forgot-email" class="placeholder-hidden">Your Email</label>
                <input type="text" class="form-control" name = "email" placeholder="E-mail" tabindex="2">
                <input name="password1" type="hidden" placeholder = "Password" class="form-control">
            </div> <!-- /.form-group -->

            <div class="form-group">
                <button type="submit" class="btn btn-secondary btn-block btn-lg" tabindex="3">
                    Reset Password &nbsp; <i class="fa fa-refresh"></i>
                </button>
            </div> <!-- /.form-group -->

            <div class="form-group">
                <a href="{{url('/login')}}"><i class="fa fa-angle-double-left"></i> &nbsp;Back to Login</a>
            </div> <!-- /.form-group -->
        </form>

    </div> <!-- /.account-body -->
</div> <!-- /.account-wrapper -->




<footer id="footer" class="midnight-blue">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 text-center">
                <p style = "margin-top:12px;">&copy; 2017, All Rights Reserved.</p>
            </div>
            <div class="col-md-9 col-sm-6 text-right">
                <ul class="fadeInDown">
                    <li><a class="" data-toggle = "modal" data-target="#privacy">Privacy Policy</a></li>
                    <li><a class="" data-toggle = "modal" data-target="#disclaimer">Disclaimer</a></li>
                    <li><a class="" data-target="#legality" data-toggle="modal">Legality</a></li>
                    <li><a href="#" target = "_blank">Join on Telegram</a></li>
                    <li><a href="{{ url('/contact') }}">Contact Us</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer><!--/#footer-->

<div class = "modal fade" id="privacy" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="vertical-alignment-helper" >
        <div class="modal-dialog vertical-align-center">
            <div class = "modal-dialog">
                <div class="modal-content">
                    <div class = "modal-header">
                        <button class = "close" data-dismiss = "modal">&times;</button>
                        <h2 class = "text-center">Privacy Policy</h2>
                    </div>
                    <div class = "modal-body">
                        <h3>Your Privacy is paramount</h3>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem. Donec bibendum egestas ipsum vel lacinia. Cras ut lacinia arcu. Morbi quis efficitur odio. Cras rhoncus orci at justo molestie, a vestibulum leo rhoncus. Morbi efficitur fermentum aliquet. Aliquam blandit, nulla at rhoncus placerat, erat lacus auctor orci, eu rutrum tellus lectus non lectus. Sed mollis elit eget arcu tempus, quis ultrices magna lacinia. Nunc interdum ipsum eros, sed posuere velit ornare in. Vestibulum lacus urna, fringilla eu dapibus id, sollicitudin sit amet velit. Vestibulum id libero nec metus accumsan imperdiet. Pellentesque dolor tellus, cursus id tortor pellentesque, vestibulum efficitur magna. Nunc viverra rhoncus gravida.
                        <h3>Web Security</h3>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem. Donec bibendum egestas ipsum vel lacinia. Cras ut lacinia arcu. Morbi quis efficitur odio. Cras rhoncus orci at justo molestie, a vestibulum leo rhoncus. Morbi efficitur fermentum aliquet. Aliquam blandit, nulla at rhoncus placerat, erat lacus auctor orci, eu rutrum tellus lectus non lectus. Sed mollis elit eget arcu tempus, quis ultrices magna lacinia. Nunc interdum ipsum eros, sed posuere velit ornare in. Vestibulum lacus urna, fringilla eu dapibus id, sollicitudin sit amet velit. Vestibulum id libero nec metus accumsan imperdiet. Pellentesque dolor tellus, cursus id tortor pellentesque, vestibulum efficitur magna. Nunc viverra rhoncus gravida.
                        <p>YLorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem. Donec bibendum egestas ipsum vel lacinia. Cras ut lacinia arcu. Morbi quis efficitur odio. Cras rhoncus orci at justo molestie, a vestibulum leo rhoncus. Morbi efficitur fermentum aliquet. Aliquam blandit, nulla at rhoncus placerat, erat lacus auctor orci, eu rutrum tellus lectus non lectus. Sed mollis elit eget arcu tempus, quis ultrices magna lacinia. Nunc interdum ipsum eros, sed posuere velit ornare in. Vestibulum lacus urna, fringilla eu dapibus id, sollicitudin sit amet velit. Vestibulum id libero nec metus accumsan imperdiet. Pellentesque dolor tellus, cursus id tortor pellentesque, vestibulum efficitur magna. Nunc viverra rhoncus gravida.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class = "modal fade" id="legality" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="vertical-alignment-helper" >
        <div class="modal-dialog vertical-align-center">
            <div class = "modal-dialog">
                <div class="modal-content">
                    <div class = "modal-header">
                        <button class = "close" data-dismiss = "modal">&times;</button>
                        <h2 class = "text-center">Legality</h2>
                    </div>
                    <div class = "modal-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem. Donec bibendum egestas ipsum vel lacinia. Cras ut lacinia arcu. Morbi quis efficitur odio. Cras rhoncus orci at justo molestie, a vestibulum leo rhoncus. Morbi efficitur fermentum aliquet. Aliquam blandit, nulla at rhoncus placerat, erat lacus auctor orci, eu rutrum tellus lectus non lectus. Sed mollis elit eget arcu tempus, quis ultrices magna lacinia. Nunc interdum ipsum eros, sed posuere velit ornare in. Vestibulum lacus urna, fringilla eu dapibus id, sollicitudin sit amet velit. Vestibulum id libero nec metus accumsan imperdiet. Pellentesque dolor tellus, cursus id tortor pellentesque, vestibulum efficitur magna. Nunc viverra rhoncus gravida.</p>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem. Donec bibendum egestas ipsum vel lacinia. Cras ut lacinia arcu. Morbi quis efficitur odio. Cras rhoncus orci at justo molestie, a vestibulum leo rhoncus. Morbi efficitur fermentum aliquet. Aliquam blandit, nulla at rhoncus placerat, erat lacus auctor orci, eu rutrum tellus lectus non lectus. Sed mollis elit eget arcu tempus, quis ultrices magna lacinia. Nunc interdum ipsum eros, sed posuere velit ornare in. Vestibulum lacus urna, fringilla eu dapibus id, sollicitudin sit amet velit. Vestibulum id libero nec metus accumsan imperdiet. Pellentesque dolor tellus, cursus id tortor pellentesque, vestibulum efficitur magna. Nunc viverra rhoncus gravida.</p>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem. Donec bibendum egestas ipsum vel lacinia. Cras ut lacinia arcu. Morbi quis efficitur odio. Cras rhoncus orci at justo molestie, a vestibulum leo rhoncus. Morbi efficitur fermentum aliquet. Aliquam blandit, nulla at rhoncus placerat, erat lacus auctor orci, eu rutrum tellus lectus non lectus. Sed mollis elit eget arcu tempus, quis ultrices magna lacinia. Nunc interdum ipsum eros, sed posuere velit ornare in. Vestibulum lacus urna, fringilla eu dapibus id, sollicitudin sit amet velit. Vestibulum id libero nec metus accumsan imperdiet. Pellentesque dolor tellus, cursus id tortor pellentesque, vestibulum efficitur magna. Nunc viverra rhoncus gravida.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class = "modal fade" id="disclaimer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="vertical-alignment-helper" >
        <div class="modal-dialog vertical-align-center">
            <div class = "modal-dialog">
                <div class="modal-content">
                    <div class = "modal-header">
                        <button class = "close" data-dismiss = "modal">&times;</button>
                        <h2 class = "text-center">Disclaimer</h2>
                    </div>
                    <div class = "modal-body">
                        <h3>Risk Disclaimer</h3>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem. Donec bibendum egestas ipsum vel lacinia. Cras ut lacinia arcu. Morbi quis efficitur odio. Cras rhoncus orci at justo molestie, a vestibulum leo rhoncus. Morbi efficitur fermentum aliquet. Aliquam blandit, nulla at rhoncus placerat, erat lacus auctor orci, eu rutrum tellus lectus non lectus. Sed mollis elit eget arcu tempus, quis ultrices magna lacinia. Nunc interdum ipsum eros, sed posuere velit ornare in. Vestibulum lacus urna, fringilla eu dapibus id, sollicitudin sit amet velit. Vestibulum id libero nec metus accumsan imperdiet. Pellentesque dolor tellus, cursus id tortor pellentesque, vestibulum efficitur magna. Nunc viverra rhoncus gravida.</p>

                        <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem. Donec bibendum egestas ipsum vel lacinia. Cras ut lacinia arcu. Morbi quis efficitur odio. Cras rhoncus orci at justo molestie, a vestibulum leo rhoncus. Morbi efficitur fermentum aliquet. Aliquam blandit, nulla at rhoncus placerat, erat lacus auctor orci, eu rutrum tellus lectus non lectus. Sed mollis elit eget arcu tempus, quis ultrices magna lacinia. Nunc interdum ipsum eros, sed posuere velit ornare in. Vestibulum lacus urna, fringilla eu dapibus id, sollicitudin sit amet velit. Vestibulum id libero nec metus accumsan imperdiet. Pellentesque dolor tellus, cursus id tortor pellentesque, vestibulum efficitur magna. Nunc viverra rhoncus gravida.</p>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem. Donec bibendum egestas ipsum vel lacinia. Cras ut lacinia arcu. Morbi quis efficitur odio. Cras rhoncus orci at justo molestie, a vestibulum leo rhoncus. Morbi efficitur fermentum aliquet. Aliquam blandit, nulla at rhoncus placerat, erat lacus auctor orci, eu rutrum tellus lectus non lectus. Sed mollis elit eget arcu tempus, quis ultrices magna lacinia. Nunc interdum ipsum eros, sed posuere velit ornare in. Vestibulum lacus urna, fringilla eu dapibus id, sollicitudin sit amet velit. Vestibulum id libero nec metus accumsan imperdiet. Pellentesque dolor tellus, cursus id tortor pellentesque, vestibulum efficitur magna. Nunc viverra rhoncus gravida.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem. Donec bibendum egestas ipsum vel lacinia. Cras ut lacinia arcu. Morbi quis efficitur odio. Cras rhoncus orci at justo molestie, a vestibulum leo rhoncus. Morbi efficitur fermentum aliquet. Aliquam blandit, nulla at rhoncus placerat, erat lacus auctor orci, eu rutrum tellus lectus non lectus. Sed mollis elit eget arcu tempus, quis ultrices magna lacinia. Nunc interdum ipsum eros, sed posuere velit ornare in. Vestibulum lacus urna, fringilla eu dapibus id, sollicitudin sit amet velit. Vestibulum id libero nec metus accumsan imperdiet. Pellentesque dolor tellus, cursus id tortor pellentesque, vestibulum efficitur magna. Nunc viverra rhoncus gravida.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="static/js/jquery.min.js"></script>
<script src="static/js/bootstrap.min.js"></script>
<script src="static/js/main.js"></script>
<script src="static/js/wow.min.js"></script>
</body>
</html>