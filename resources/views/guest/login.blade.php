@extends('layout.access')

@section('login')
    @if(session()->has('message'))
        <section class="content-header">
            <div class="row">
                <div class="col col-md-12">
                    <div class="alert alert-danger">
                        <div class="col-md-offset-2">
                            <span>{{ session()->get('message') }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif
        <!-- BEGIN LOGIN FORM -->
<form class="login-form" action="{{ route('login') }}" method="post">
    {{ csrf_field() }}
    <h3 class="form-title font-green">Sign In</h3>

    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">Username</label>
        <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="user_name" />
    </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Password</label>
        <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" />
    </div>
    <div class="form-actions">
        <button type="submit" class="btn green uppercase">Login</button>
        <label class="rememberme check mt-checkbox mt-checkbox-outline">
            <input type="checkbox" name="remember" value="1" />Remember
            <span></span>
        </label>
        <a href="{{url('password-reset')}}" class="forget-password">Forgot Password?</a>
    </div>
    <div class="create-account">
        <p>
            <a href="{{ route('register') }}" class="uppercase">Create an account</a>
        </p>
    </div>
</form>
<!-- END LOGIN FORM -->
@endsection