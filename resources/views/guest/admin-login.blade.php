@extends('layout.access')

@section('admin-login')
        <!-- BEGIN LOGIN FORM -->
<form class="login-form" action="{{ route('admin-login') }}" method="post">
    {{ csrf_field() }}
    <h3 class="form-title font-green">Welcome Admin</h3>

    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">Username</label>
        <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="user_name" />
    </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Password</label>
        <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" />
    </div>
    <div class="form-actions">
        <button type="submit" class="btn green">Sign in</button>
    </div>
</form>
<!-- END LOGIN FORM -->
@endsection