@extends('layout.access')

@section('register')
    <!-- BEGIN REGISTRATION FORM -->
<form class="login-form" action="{{ route('register') }}" method="post">
    {{ csrf_field() }}

    <h3 class="font-green">Sign Up</h3>
    <p class="hint"> Enter your personal details below: </p>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Email</label>
        <input class="form-control placeholder-no-fix" type="text" placeholder="Email" name="email" required />
    </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Phone</label>
        <input class="form-control placeholder-no-fix" type="text" maxlength = "10" placeholder="Phone No.(5559990000)" name="phone" required />
    </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">City</label>
        <input class="form-control placeholder-no-fix" type="text" placeholder="City" name="region" />
    </div>
    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">Payment Name</label>
        <input class="form-control placeholder-no-fix" type="text" placeholder="Payment Name" name="payment_name" required />
    </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Network</label>
        <select class="form-control" name="payment_method" id="payment_method" required tabindex="7">
            <option selected = "selected" disabled = "disabled" value="">Select Payment Method</option>
            @foreach($methods as $key => $value)
                <option value="{{ $key }}">{{ $value }} </option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">Payment Number</label>
        <input class="form-control placeholder-no-fix" type="text" placeholder="Payment Number" maxlength = "10" name="payment_number" required />
    </div>
    @if(is_null($source ))
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Referral</label>
            <input class="form-control placeholder-no-fix" type="hidden" value="admin" name="guide"/>
        </div>
    @else
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Referral</label>
            <input class="form-control placeholder-no-fix" type="hidden" value="{{ $source }}" name="guide"/>
        </div>
    @endif

    <p class="hint"> Enter your account details below: </p>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Username</label>
        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="user_name" required/>
    </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Password</label>
        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="Password" name="password" required/>
    </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>
        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Re-type Your Password" name="password_confirmation" required/>
    </div>

    <div class="form-group margin-top-20 margin-bottom-20">
        <label class="mt-checkbox mt-checkbox-outline">
            <input type="checkbox" name="terms" required> I agree to the
            <a  data-toggle="modal" data-target="#disclaimer">Terms of Service </a> &
            <a  data-toggle="modal" data-target="#privacy">Privacy Policy </a>
            <span></span>
        </label>
        <div class = "modal fade" id="disclaimer" role="dialog" aria-hidden="true">
            <div class="vertical-alignment-helper" >
                <div class="modal-dialog vertical-align-center">
                    <div class = "modal-dialog">
                        <div class="modal-content">
                            <div class = "modal-header">
                                <button class = "close" data-dismiss = "modal">&times;</button>
                                <h2 class = "text-center">Disclaimer</h2>
                            </div>
                            <div class = "modal-body">
                                <h3>Risk Disclaimer</h3>

                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem. Donec bibendum egestas ipsum vel lacinia. Cras ut lacinia arcu. Morbi quis efficitur odio. Cras rhoncus orci at justo molestie, a vestibulum leo rhoncus.</p>

                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem. Donec bibendum egestas ipsum vel lacinia. Cras ut lacinia arcu. Morbi quis efficitur odio. Cras rhoncus orci at justo molestie, a vestibulum leo rhoncus.</p>

                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem. Donec bibendum egestas ipsum vel lacinia. Cras ut lacinia arcu. Morbi quis efficitur odio. Cras rhoncus orci at justo molestie, a vestibulum leo rhoncus.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem. Donec bibendum egestas ipsum vel lacinia. Cras ut lacinia arcu. Morbi quis efficitur odio. Cras rhoncus orci at justo molestie, a vestibulum leo rhoncus.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class = "modal fade" id="privacy"  role="dialog" aria-hidden="true">
            <div class="vertical-alignment-helper" >
                <div class="modal-dialog vertical-align-center">
                    <div class = "modal-dialog">
                        <div class="modal-content">
                            <div class = "modal-header">
                                <button class = "close" data-dismiss = "modal">&times;</button>
                                <h2 class = "text-center">Privacy Policy</h2>
                            </div>
                            <div class = "modal-body">
                                <h3>Your Privacy is paramount</h3>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem. Donec bibendum egestas ipsum vel lacinia. Cras ut lacinia arcu. Morbi quis efficitur odio. Cras rhoncus orci at justo molestie, a vestibulum leo rhoncus.
                                <h3>Web Security</h3>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem. Donec bibendum egestas ipsum vel lacinia. Cras ut lacinia arcu. Morbi quis efficitur odio. Cras rhoncus orci at justo molestie, a vestibulum leo rhoncus.
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem. Donec bibendum egestas ipsum vel lacinia. Cras ut lacinia arcu. Morbi quis efficitur odio. Cras rhoncus orci at justo molestie, a vestibulum leo rhoncus.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <button type="submit" class="btn btn-success uppercase">Create My Account</button>
    </div>
    <div class="create-account">
        <p>
            <a href="{{ route('login') }}" class="uppercase">Login</a>
        </p>
    </div>
</form>

    <!-- END REGISTRATION FORM -->
@endsection