@extends('layout.external-main')

@section('home')
        <!-- BEGIN BANNER -->
<div class="banner">
    <div class="container">

        <h1 class="banner-title-main"><span class="banner-title-bold">c o m m u n i f i e d</span></h1>

        <p class="banner-title-paragraph">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem. Donec bibendum egestas ipsum vel lacinia. Cras ut lacinia arcu. Morbi quis efficitur odio. Cras rhoncus orci at justo molestie, a vestibulum leo rhoncus.</p>
        <a data-hint="tooltip" data-original-title="Join Community" data-placement="top" href="#">
            <span class="banner-quote" style="background-color: #32c5d2">Join the community today</span>
        </a>
        <br/>
    </div>
    <div class="container" style="margin-top: 30px;">
    </div>
</div>
<!-- END BANNER -->
<!-- BEGIN CUSTOMER FEEDBACK -->
<div class="container">
    <div class="customer-feedback">
        <center>
            <h3 class="title-normal" style="margin-bottom: 50px;">A Member's Testimony</h3>
        </center>

        <div style="">
            <p class="customer-quote">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem. Donec bibendum egestas ipsum vel lacinia. Cras ut lacinia arcu. Morbi quis efficitur odio. Cras rhoncus orci at justo molestie, a vestibulum leo rhoncus. <br/>
                Thank you Communified!
            </p>
            <span class="customer-name" style="background-color: #32c5d2">Some random member name</span>
        </div>

    </div>
</div>
<!-- END CUSTOMER FEEDBACK -->
@endsection
