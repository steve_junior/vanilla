@extends('layout.admin-layout')

@section('resolve-conflict')
        <!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Resolve Conflicts</h3>
                </div>
                @if($records->count() > 0)
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>By Date</th>
                                    <th>Sender Name</th>
                                    <th>Sender Number</th>
                                    <th>Amount</th>
                                    <th>Recipient Name</th>
                                    <th>Recipient Number</th>
                                    <th>Evidence</th>
                                    <th>Perform Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($records as $record)
                                <tr>
                                    <td>{{ $record->payment_completed_by->toDayDateTimeString() }}</td>
                                    <td>{{ $record->sender_name }}</td>
                                    <td>{{ $record->sender_number }}</td>
                                    <td>{{ $record->amount }}</td>
                                    <td>{{ $record->receiver_name }}</td>
                                    <td>{{ $record->receiver_number }}</td>
                                    <td>
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#view-proof-modal">
                                            show
                                        </button>
                                        <div class="modal fade" id="view-proof-modal">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <img src="{{ asset($record->proof_of_payment) }}" alt="Proof">
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-toolbar">
                                            <div class="btn-group btn-group-sm">
                                                <button type="button" class="btn btn-success glyphicon glyphicon-edit" data-toggle="modal" data-target="#confirm-transaction"></button>
                                                <button type="button" class="btn btn-danger glyphicon glyphicon-info-sign" data-toggle="modal" data-target="#decline-transaction"></button>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <div class="modal fade" id="confirm-transaction">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title">Confirm Transaction</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>Are you sure  you  want to confirm transfer of <b>{{ $record->amount }}c </b> from <b>{{ $record->sender_name }}</b> / <b>{{ $record->sender_number }}</b> to <b>{{ $record->receiver_name }}</b> / <b>{{ $record->receiver_number }}</b>  ? </p>
                                            </div>
                                            <div class="modal-footer">
                                                <form action="{{ route('admin-confirm-transaction') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="record_id" value="{{ $record->id }}">
                                                    <input type="hidden" name="confirmation" value={{ true }}>
                                                    <input type="hidden" name="declination" value={{ false }}>

                                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" class="btn btn-success pull-right">Confirm</button>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <div class="modal fade" id="decline-transaction">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title">Decline Transaction</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>Are you sure you want to decline transfer of <b> {{ $record->amount }}c </b> from <b>{{ $record->sender_name }}</b> / <b>{{ $record->sender_number }} </b> to <b> {{ $record->receiver_name }}</b> / <b>{{ $record->receiver_number }} </b> ?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <form action="{{ route('admin-decline-transaction') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="record_id" value="{{ $record->id }}">
                                                    <input type="hidden" name="confirmation" value={{ false }}>
                                                    <input type="hidden" name="declination" value={{ true }}>

                                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" class="btn btn-danger pull-right">
                                                        Decline
                                                    </button>
                                                </form>
                                                <!--button type="submit" class="btn btn-danger" data-dismiss="modal" onclick="event.preventDefault();document.getElementById('decline_trans_form').submit();">Decline</button-->
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <style>
                        .content {
                            text-align: center;
                        }
                        div{
                            display: block;
                        }
                        .title {
                            font-size: 36px;
                            padding: 20px;
                        }
                    </style>
                    <div class="content">
                        <div class="title">No conflicts Yet</div>
                    </div>
                    @endif
                            <!-- /.box-header -->

                    <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
@endsection