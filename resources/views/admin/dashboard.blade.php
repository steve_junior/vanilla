@extends('layout.admin-layout')

@section('dashboard')
    <section class="content">
        <div class="callout callout-success">
            <h4>Lorem ipsum!</h4>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque,
                nec placerat nunc massa non quam. Duis et rhoncus sem.
            </p>
        </div>

        @if($PayIns != false)
            <div class="row">
                <div class="col-md-12 col-md-offset-0">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Pay Up Quickly! </h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit.
                            <table class="table table-bordered">
                                <tr>
                                    <th>Name</th>
                                    <th>Contact</th>
                                    <th>Amount</th>
                                    <th>Time Left</th>
                                    <th>Actions</th>
                                </tr>
                                @foreach($PayIns as $item)
                                    <tr>
                                        <td>{{ $item->sender_name }}</td>
                                        <td>{{ $item->sender_number }}</td>
                                        <td>c {{ $item->amount }}</td>
                                        <td>
                                            {{--@if(\Carbon\Carbon::now()->diffInSeconds( $item->payment_completed_by, false ) < 0 )--}}
                                                {{--<form action="{{ route('/') }}">--}}
                                                    {{--<input type="hidden" name="match_id" value="{{ $item->id }}"/>--}}
                                                    {{--<button class="btn btn-success" type="submit">Request rematching</button>--}}
                                                {{--</form>--}}
                                            @if(\Carbon\Carbon::now()->diffInHours($item->payment_completed_by) > 1)
                                                <div>&lt {{ Carbon\Carbon::now()->diffInHours($item->payment_completed_by) }}h</div>
                                            @else <div>&lt {{ Carbon\Carbon::now()->diffInMinutes($item->payment_completed_by) }}m</div>

                                            @endif
                                        </td>
                                        <td>
                                            @if($item->has_donor_paid and $item->payment_declined == false)
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#confirm-modal">Confirm</button>
                                                    <button type="button" class="btn btn-danger btn-flat" data-toggle="modal" data-target="#decline-modal">Decline</button>
                                                </div>
                                            @elseif($item->payment_declined)
                                                <span>Awaiting Resolution...</span>
                                            @else
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#extend-time-modal">Extend time</button>
                                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#confirm-modal">Confirm</button>
                                                    <button type="button" class="btn btn-danger btn-flat" data-toggle="modal" data-target="#decline-modal">Decline</button>
                                                </div>
                                            @endif
                                        </td>
                                    </tr>
                                    <div class="modal fade" id="confirm-modal">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <h4 class="modal-title">Confirm Payment</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Are you sure that you received a payment of {{ $item->amount }} from {{ $item->sender_name }} ?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <form action="{{ route('confirm-payment') }}" method="POST">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="match_id" value="{{ $item->id }}">
                                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                                                        <button type="submit" class="btn btn-success">Confirm</button>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <div class="modal fade" id="decline-modal">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">Decline Payment</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Are you sure you want to decline payment from {{ $item->sender_name }} by Account Number {{ $item->sender_number }}</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <form action="{{ route('decline-payment') }}" method="POST">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="match_id" value="{{ $item->id }}">

                                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                                                        <button type="submit" class="btn btn-danger">Decline</button>
                                                    </form>

                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                @endforeach
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
        @endif
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Lorem ipsum</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="" data-toggle="tooltip" title="Refresh">
                            <i class="fa fa-refresh"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>

                <div class="box-body">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem.
                </div>
                <!-- /.box-body -->
            </div>

    </section>
@endsection