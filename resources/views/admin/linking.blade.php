@extends('layout.admin-layout')

@section('linking')
        <!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Donors</h3>
                </div>

                @if($donors->count() > 0)
                    <div class="box-body">
                        <table id="table1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Amount</th>
                                    <th>Type</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($donors as $donor)
                                <tr>
                                    <td>{{ $donor->users->user_name }}</td>
                                    <td>{{ $donor->amount_left }}</td>
                                    @if($donor->is_remnant)
                                        <td><button class="btn btn-primary"><span>60 %</span></button></td>
                                        @else
                                        <td><button class="btn btn-success"><span>Recommit</span></button> </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <style>
                        .content {
                            text-align: center;
                        }
                        div{
                            display: block;
                        }
                        .title {
                            font-size: 36px;
                            padding: 20px;
                        }
                    </style>
                    <div class="content">
                        <div class="title">No Donors Yet</div>
                    </div>
                    @endif
                            <!-- /.box-header -->

                    <!-- /.box-body -->
            </div>

            <div class="pull-left">
                <button class="btn btn-primary"><span>{{ $donors->sum('amount_left') }}</span></button>
            </div>
            <!-- /.box -->
        </div>
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Benefactors</h3>
                </div>

                @if($benefactors->count() > 0)
                    <div class="box-body">
                        <table id="table2" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($benefactors as $benefactor)
                                    <tr>
                                        <td>{{ $benefactor->users->user_name }}</td>
                                        <td>{{ $benefactor->amount_left }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="pull-right">
                        <form method="post" action="{{ route('admin-match') }}">
                            {{ csrf_field() }}
                            <button class="btn btn-success" type="submit">Launch</button>
                        </form>
                    </div>
                @else
                    <style>
                        .content {
                            text-align: center;
                        }
                        div{
                            display: block;
                        }
                        .title {
                            font-size: 36px;
                            padding: 20px;
                        }
                    </style>
                    <div class="content">
                        <div class="title">No Benefactors Yet</div>
                    </div>
                 @endif
                            <!-- /.box-header -->

                    <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
@endsection