@extends('layout.admin-layout')

@section('configs')
<!-- Input addon -->
    <div class="box box-info" style="width: 50%;">
    <div class="box-header with-border">
        <h3 class="box-title">System Configurations</h3>
    </div>
    <div class="box-body">
        <form method="POST" class="form account-form" role="form" action = '{{ route('configurations') }}'>
            {{ csrf_field() }}
            <div class="input-group form-group">
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>
                <label for="maturity_period">Maturity Period</label>
                <div class="form-group">
                    <input id="maturity_period" type="number" class="form-control" name="maturity_period" value="{{ $config['maturity_period'] }}">
                </div>
            </div>
            <br>
            <div class="input-group form-group">
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>
                <label for="first_percent">Initial Proportion</label>
                <div class="form-group">
                    <input id="first_percent" type="number" class="form-control" name="first_percentage" value="{{ $config['first_percentage'] }}">
                </div>
            </div>
            <br>
            <div class="input-group form-group">
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>
                <label for="final_percent">Final Proportion</label>
                <div class="form-group">
                    <input id="final_percent" type="number" class="form-control" name="remaining_percentage" value="{{ $config['remaining_percentage'] }}">
                </div>
            </div>
            <br>
            <div class="input-group form-group">
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>
                <label for="allow_recom_period">Hours Before Recommitment</label>
                <div class="form-group">
                    <input id="allow_recom_period" type="number" class="form-control" name="allow_recommitment_period" value="{{ $config['allow_recommitment_period'] }}">
                </div>
            </div>
            <br>
            <div class="input-group form-group">
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>
                <label for="transaction_lap">Transaction Lap Hours</label>
                <div class="form-group">
                    <input id="transaction_lap" type="number" class="form-control" name="transaction_lap_hours" value="{{ $config['transaction_lap_hours'] }}">
                </div>
            </div>
            <br>
            <div class="input-group form-group">
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>
                <label for="min_donation">Minimum Donation</label>
                <div class="form-group">
                    <input id="min_donation" min="100" type="number" class="form-control" name="minimum_donation" value="{{ $config['minimum_donation'] }}">
                </div>
            </div>
            <br>
            <div class="input-group form-group">
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>
                <label for="max_donation">Maximum Donation</label>
                <div class="form-group">
                    <input id="max_donation" type="number" class="form-control" name="maximum_donation" value="{{ $config['maximum_donation'] }}">
                </div>
            </div>
            <br>

            <div class="input-group form-group">
                <span class="input-group-addon"><i class="fa fa-phone-square"></i></span>
                <label for="factor">Factor</label>
                <div class="form-group">
                    <input id="factor" type="text" name="factor" value="{{ $config['factor'] }}" class="form-control">
                </div>
            </div>
            <br>
            <div class="form-group">
                <button type="button" data-toggle="modal" data-target="#update-config" class="btn btn-danger btn-block btn-lg" tabindex="4">
                    Modify Configurations
                </button>
            </div> <!-- /.form-group -->
            <!-- /input-group -->
            <div class="modal fade" id="update-config">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Modify Configurations</h4>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure  you  want to <b>modify</b> the system configurations ?</p>
                            <br>
                            <p><b>Note that this action once performed will take effect immediately.</b></p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-danger pull-right">Modify</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        </form>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

@endsection