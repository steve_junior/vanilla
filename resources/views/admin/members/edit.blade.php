@extends('layout.admin-layout')

@section('manage-members-edit')

    <!-- Input addon -->
    <div class="box box-info" style="width: 50%;">
        <div class="box-header with-border">
            <h3 class="box-title">Update Membership Details</h3>
        </div>
        <div class="box-body">
            <form method="POST" class="form account-form" role="form" action = '{{ route('admin-update-member', $member->id) }}'>
                {{ csrf_field() }}
                <div class="input-group form-group">
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>
                    <label for="user_name">Username</label>
                    <div class="form-group">
                        <input id="user_name" type="text" class="form-control" name="user_name" value="{{ $member->user_name }}" required>
                    </div>
                </div>
                <br>
                <div class="input-group form-group">
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>
                    <label for="email">Email</label>
                    <div class="form-group">
                        <input id="email" type="email" class="form-control" name="email" value="{{ $member->email }}" required>
                    </div>
                </div>
                <br>
                <div class="input-group form-group">
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>
                    <label for="phone">Phone</label>
                    <div class="form-group">
                        <input id="phone" type="tel" class="form-control" name="phone" value="{{ $member->phone }}" required>
                    </div>
                </div>
                <br>
                <div class="input-group form-group">
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>
                    <label for="payment_name">Payment Name</label>
                    <div class="form-group">
                        <input id="payment_name" type="text" class="form-control" name="payment_name" value="{{ $member->payment_name }}" required>
                    </div>
                </div>
                <div class="input-group form-group">
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>
                    <label for="payment_number">Payment Number</label>
                    <div class="form-group">
                        <input id="payment_number" type="tel" class="form-control" name="payment_number" value="{{ $member->payment_number }}" required>
                    </div>
                </div>
                <br>

                <div class="input-group form-group">
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>
                    <label for="region">Region</label>
                    <div class="form-group">
                        <input id="region" type="text" class="form-control" name="region" value="{{ $member->region }}" required>
                    </div>
                </div>
                <br>

                <div class="form-group">
                    <button type="button" data-toggle="modal" data-target="#update-member" class="btn btn-danger btn-block btn-lg" tabindex="4">
                        Update Membership Details
                    </button>
                </div> <!-- /.form-group -->
                <!-- /input-group -->
                <div class="modal fade" id="update-member">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Modify Configurations</h4>
                            </div>
                            <div class="modal-body">
                                <p>Are you sure  you  want to <b>modify</b> the membership details ?</p>
                                <br>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-danger pull-right">Update</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection