@extends('layout.admin-layout')

@section('manage-members')
        <!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Communified Members</h3>
                </div>
                @if($members->count() > 0)
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Contact</th>
                                <th>Account Name</th>
                                <th>Account Number</th>
                                <th>Last Activity</th>
                                <th>Perform Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($members as $member)
                                <tr>
                                    <td>{{ $member['user_name'] }}</td>
                                    <td>{{ $member['email'] }}</td>
                                    <td>{{ $member['phone'] }}</td>
                                    <td>{{ $member['payment_name'] }}</td>
                                    <td>{{ $member['payment_number'] }}</td>
                                    <td>{{ \Illuminate\Support\Carbon::parse($member['last_logged_at'])->diffForHumans() }}</td>
                                    <td>
                                        <div class="btn-toolbar">
                                            <div class="btn-group">
                                                <a href="{{ route('admin-edit-member', $member['id']) }}" class="btn btn-default glyphicon glyphicon-edit"></a>
                                                <a href="{{ route('admin-view-member', $member['id']) }}" class="btn btn-info glyphicon glyphicon-info-sign"></a>
                                                <button type="button" class="btn btn-success glyphicon glyphicon-ok" data-toggle="modal" data-target="#confirm-member-{{ $member->id }}"></button>
                                                <button type="button" class="btn btn-warning glyphicon glyphicon-refresh" data-toggle="modal" data-target="#unbar-member-{{ $member->id }}"></button>
                                                <button type="button" class="btn btn-danger glyphicon glyphicon-trash" data-toggle="modal" data-target="#delete-member-{{ $member->id }}"></button>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <div class="modal fade" id="confirm-member-{{ $member->id }}">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title">Confirm Account</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>Are you sure  you  want to <b>confirm</b> member by Payment name <b>{{ $member['payment_name'] }}</b> and number <b>{{ $member['payment_number'] }}</b> under the username <b>{{ $member['user_name'] }}</b>?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <form action="{{ route('admin-confirm-member') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="member_id" value="{{ $member['id'] }}">
                                                    <input type="hidden" name="email" value={{ $member['email'] }}>
                                                    <input type="hidden" name="user_name" value={{ $member['user_name'] }}>

                                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" class="btn btn-success pull-right">Confirm</button>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <div class="modal fade" id="unbar-member-{{ $member->id }}">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title">Unbar Account</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>Are you sure  you  want to <b>unblock</b> account by Payment name <b>{{ $member['payment_name'] }}</b> and number <b>{{ $member['payment_number'] }}</b> under the username <b>{{ $member['user_name'] }}</b>?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <form action="{{ route('admin-unbar-member') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="member_id" value="{{ $member['id'] }}">
                                                    <input type="hidden" name="email" value={{ $member['email'] }}>
                                                    <input type="hidden" name="user_name" value={{ $member['user_name'] }}>

                                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" class="btn btn-warning pull-right">Unblock</button>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <div class="modal modal-danger fade" id="delete-member-{{ $member->id }}">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title">Remove Account</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>Are you sure  you  want to <b>remove</b> account by Payment name <b>{{ $member['payment_name'] }}</b> and number <b>{{ $member['payment_number'] }}</b> under the username <b>{{ $member['user_name'] }}</b> from the system?</p>
                                                <br>
                                                <p><b>Note that this action once performed cannot be reversed.</b></p>
                                            </div>
                                            <div class="modal-footer">
                                                <form action="{{ route('admin-unbar-member') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="member_id" value="{{ $member['id'] }}">
                                                    <input type="hidden" name="email" value={{ $member['email'] }}>
                                                    <input type="hidden" name="user_name" value={{ $member['user_name'] }}>

                                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" class="btn btn-danger pull-right">Unblock</button>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <style>
                        .content {
                            text-align: center;
                        }
                        div{
                            display: block;
                        }
                        .title {
                            font-size: 36px;
                            padding: 20px;
                        }
                    </style>
                    <div class="content">
                        <div class="title">No Members Yet</div>
                    </div>
                    @endif
                            <!-- /.box-header -->

                    <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
@endsection
