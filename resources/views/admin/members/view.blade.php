@extends('layout.admin-layout')

@section('manage-members-view')
       <div class="box box-primary" style="width: 50%;">
           <div class="box-body box-profile">
               <img class="profile-user-img img-responsive img-circle" src="dist/img/p2p2/user_128.png" alt="User profile picture">

               <h3 class="profile-username text-center">{{ $member->user_name }}</h3>

               <ul class="list-group list-group-unbordered">
                   <li class="list-group-item">
                       <b>Email Address <a class="pull-right">{{ $member->email }}</a></b>
                   </li>
                   <li class="list-group-item">
                       <b>Phone Number <div class="pull-right">{{ $member->phone }}</div></b>
                   </li>
                   <li class="list-group-item">
                       <b>Account Number <div class="pull-right">{{ $member->payment_number }}</div></b>
                   </li>
                   <li class="list-group-item">
                       <b>Account <div class="pull-right">{{ $member->payment_name }}</div></b>
                   </li>
                   <li class="list-group-item">
                       <b>Credit Network <div class="pull-right">{{ $member->payment_method }}</div></b>
                   </li>
                   <li class="list-group-item">
                       <b>Region <div class="pull-right">{{ $member->region }}</div></b>
                   </li>
                   <li class="list-group-item">
                       @if(is_null($member->donations->last()))
                           <b>Current Pledge <div class="pull-right">No Donation Yet</div></b>
                       @else
                           <b>Current Pledge <div class="pull-right">c {{ $member->donations->last()->deposited_amount }}</div></b>
                       @endif
                   </li>
                   <li class="list-group-item">
                       <b>Last Login <div class="pull-right">{{ \Illuminate\Support\Carbon::parse($member->last_logged_at)->diffForHumans() }}</div></b>
                   </li>
                   <li class="list-group-item">
                       @if($member->is_barred == 1)
                           <b>Member Barred Status<div class="pull-right">Blocked</div></b>
                       @else <b>Member Barred Status<div class="pull-right">Unblocked</div></b>
                       @endif
                   </li>
                   <li class="list-group-item">
                       <b>Member Activity Status
                       @if($member->is_confirmed == 1)
                           <div class="pull-right">Active</div></b>
                       @else <div class="pull-right">Inactive</div></b>
                       @endif
                   </li>
                   <li class="list-group-item">
                       <b>Joined Community <div class="pull-right">{{ \Illuminate\Support\Carbon::parse($member->created_at)->diffForHumans() }}</div></b>
                   </li>
               </ul>
           </div>
           <!-- /.box-body -->
       </div>
@endsection
