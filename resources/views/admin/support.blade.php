@extends('layout.admin-layout')

@section('support')
        <!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Support Center</h3>
                </div>
                @if($records->count() > 0)
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Deposited On</th>
                                <th>Username</th>
                                <th>Contact</th>
                                <th>Account Name</th>
                                <th>Subject</th>
                                <th>Comment</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($records as $record)
                                <tr>
                                    <td>{{ $record['created_at'] }}</td>
                                    <td>{{ $record['user_name'] }}</td>
                                    <td>{{ $record['contact'] }}</td>
                                    <td>{{ $record['account_name'] }}</td>
                                    <td>{{ $record['subject'] }}</td>
                                    <td>{{ $record['comment'] }}</td>
                                    @if( $record['status'] )
                                        <td>Resolved</td>
                                        <td>
                                            <button class="btn btn-default" type="button" data-toggle="modal" data-target="#unmark-as-resolved-modal">Unmark as resolved</button>
                                        </td>
                                        <div class="modal fade" id="unmark-as-resolved-modal">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title">Issue Resolved</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Are you sure you want to <b>unmark</b> complaint by {{ $record['user_name'] }} from resolved ?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <form action="{{ route('unresolve') }}" method="POST">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="complaint_id" value="{{ $record['id'] }}">

                                                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                                                            <button type="submit" class="btn btn-danger"> Accept </button>
                                                        </form>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                    @else <td>Pending</td>
                                          <td>
                                              <button class="btn btn-success" type="button" data-toggle="modal" data-target="#mark-as-resolved-modal">Mark as resolved</button>
                                          </td>
                                             <div class="modal fade" id="mark-as-resolved-modal">
                                                <div class="modal-dialog">
                                                <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">Issue Resolved</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Are you sure you want to <b>mark</b> complaint by {{ $record['user_name'] }} as resolved ?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <form action="{{ route('resolve') }}" method="POST">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="complaint_id" value="{{ $record['id'] }}">

                                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                                                        <button type="submit" class="btn btn-danger"> Accept </button>
                                                    </form>
                                                </div>
                                            </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <style>
                        .content {
                            text-align: center;
                        }
                        div{
                            display: block;
                        }
                        .title {
                            font-size: 36px;
                            padding: 20px;
                        }
                    </style>
                    <div class="content">
                        <div class="title">No complaints Yet</div>
                    </div>
                    @endif
                            <!-- /.box-header -->

                    <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
@endsection