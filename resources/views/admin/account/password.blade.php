@extends('layout.admin-layout')

@section('password')
        <!-- Horizontal Form -->
<div class="box box-info" style="width: 60%;">
    <div class="box-header with-border">
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form class="form-horizontal account-form" method="POST" role="form" action = '{{ route('admin-change-password') }}'>
        {{ csrf_field() }}

        <div class="box-body">
            <div class="form-group{{ $errors->has('current_password') ? ' has-error' : '' }}">
                <label for="oldPassword" class="col-sm-4 control-label">Current Password*</label>
                <div class="col-sm-6">
                    <input type="password" class="form-control" id="oldPassword" name="current_password" required>
                    @if ($errors->has('current_password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('current_password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('new_password') ? ' has-error' : '' }}">
                <label for="newPassword" class="col-sm-4 control-label">New Password*</label>

                <div class="col-sm-6">
                    <input type="password" min="4" max="16" name="new_password" class="form-control" id="newPassword" required>

                    @if ($errors->has('new_password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('new_password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('new_password_confirmation') ? ' has-error' : '' }}">
                <label for="retypePassword" class="col-sm-4 control-label">Confirm New Password*</label>

                <div class="col-sm-6">
                    <input type="password" class="form-control" id="retypePassword" name="new_password_confirmation"  required>

                    @if ($errors->has('new_password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('new_password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <div class="col-sm-offset-4">
                <button type="submit" class="btn btn-danger">Change Password</button>
            </div>
        </div>
        <!-- /.box-footer -->
    </form>
</div>
@endsection