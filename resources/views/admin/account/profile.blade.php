@extends('layout.admin-layout')

@section('profile')
        <!-- Profile Image -->
<div class="box box-primary" style="width: 60%;">
    <div class="box-body box-profile">
        <img class="profile-user-img img-responsive img-circle" src="dist/img/p2p2/user_128.png" alt="User profile picture">

        <h3 class="profile-username text-center">{{ $admin['full_name'] }}</h3>

        <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
                <b>Phone Number</b> <a class="pull-right">{{ $admin['phone'] }}</a>
            </li>
            <li class="list-group-item">
                <b>Email Address</b> <a class="pull-right">{{ $admin['email'] }}</a>
            </li>
            <li class="list-group-item">
                <b>Last Login</b> <a class="pull-right">{{ Carbon\Carbon::parse($admin['last_logged_in'])->diffForHumans() }}</a>
            </li>
        </ul>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->
@endsection