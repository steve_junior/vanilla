<!DOCTYPE html>
<html lang="en" class="">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>Communified</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta name="description" content="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem. Donec bibendum egestas ipsum vel lacinia. Cras ut lacinia arcu. Morbi quis efficitur odio. Cras rhoncus orci at justo molestie, a vestibulum leo rhoncus."/>
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />

    <link href='{{ asset('static/css/google-font-robot.min.css') }}' rel='stylesheet' type='text/css'>
    <link href="{{ asset('_start/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('_start/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('_start/style.css') }}" rel="stylesheet" type="text/css"/>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body quick-markup_injected="true">
<!-- BEGIN TOPBAR -->
<div class="topbar navbar-fixed-top">

    <a class="topbar-link" data-hint="tooltip" data-original-title="Have a concern? Get in touch with us" data-placement="bottom" href="{{ route('contact') }}"  role="button">Contact Us</a>
    <span class="topbar-sep"></span>

    <a class="topbar-link" data-hint="tooltip"  data-placement="bottom" href="#" target="_blank" role="button">Join On Telegram</a>
    <span class="topbar-sep"></span>

    <a class="topbar-link" data-hint="tooltip" data-original-title="Administrator section" data-placement="bottom" href="{{ route('admin-login') }}" role="button">Admin Login</a>
    <span class="topbar-sep"></span>

    <a class="topbar-link" data-hint="tooltip" data-original-title="View Disclaimer" data-placement="bottom" role="button" data-toggle="modal" data-target="#disclaimer">Disclaimer</a>
    <span class="topbar-sep"></span>

    <a class="topbar-link" data-hint="tooltip" data-original-title="Create an Account with Us" data-placement="bottom" href="{{ route('register') }}" role="button">Register</a>
    <span class="topbar-sep"></span>

    <a class="topbar-purchase" data-hint="tooltip" data-original-title="Login into Dashboard" data-placement="bottom" href="{{ url('/login') }}" role="button">Login</a>
    <span class="topbar-sep"></span>

    <a class="topbar-link" data-hint="tooltip" data-placement="bottom" href="{{ route('home') }}" role="button">Home</a>
</div>

<div class = "modal fade" id="disclaimer" role="dialog" aria-hidden="true">
    <div class="vertical-alignment-helper" >
        <div class="modal-dialog vertical-align-center">
            <div class = "modal-dialog">
                <div class="modal-content">
                    <div class = "modal-header">
                        <button class = "close" data-dismiss = "modal">&times;</button>
                        <h2 class = "text-center">Disclaimer</h2>
                    </div>
                    <div class = "modal-body">
                        <h3>Risk Disclaimer</h3>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem. Donec bibendum egestas ipsum vel lacinia. Cras ut lacinia arcu. Morbi quis efficitur odio. Cras rhoncus orci at justo molestie, a vestibulum leo rhoncus.</p>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem. Donec bibendum egestas ipsum vel lacinia. Cras ut lacinia arcu. Morbi quis efficitur odio. Cras rhoncus orci at justo molestie, a vestibulum leo rhoncus.</p>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem. Donec bibendum egestas ipsum vel lacinia. Cras ut lacinia arcu. Morbi quis efficitur odio. Cras rhoncus orci at justo molestie, a vestibulum leo rhoncus.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem. Donec bibendum egestas ipsum vel lacinia. Cras ut lacinia arcu. Morbi quis efficitur odio. Cras rhoncus orci at justo molestie, a vestibulum leo rhoncus.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END TOPBAR -->

<!--Place pages here-->
    @yield('home')

    @yield('support-guest')

    @yield('contact')
<!--End here-->

<!-- BEGIN FOOTER -->
<footer class="footer">
    <ul class="list-inline social-icons">
    </ul>
    <p class="copyright">Copyright &#64; <a href="#">Communified</a></p>
</footer>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS -->
<!-- BEGIN CORE PLUGINS -->
<script src="{{ asset('_start/plugins/jquery-1.11.0.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('_start/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script>
    $(document).ready(function(){
        $('[data-hint="tooltip"]').tooltip({
            container: 'body'
        })
    });
</script>
</body>
<!-- END BODY -->
</html>