<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Communified Network</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('bower_components/Ionicons/css/ionicons.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect. -->
    <link rel="stylesheet" href="{{ asset('dist/css/skins/skin-blue.min.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="">

    <!--Laravel Resources-->
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels-->
            <span class="logo-mini">P2P</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>P2P</b></span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="{{ asset('dist/img/p2p2/user_128.png') }}" class="user-image" alt="User Image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{ $name }}</span>
                        </a>
                    </li>

                    <!-- Control Sidebar Toggle Button -->
                    <li>
                        <a href="{{ route('admin-logout') }}"
                           onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <form id="logout-form" action="{{ route('admin-logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>

    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ asset('dist/img/p2p2/user_128.png') }}" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{ $name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i>{{ $ip }}</a>
            </div>
            </div>

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu" data-widget="tree">
                <!-- Optionally, you can add icons to the links -->
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-institution"></i><span>Dashboard</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('admin-system-dashboard') }}">Systems</a></li>
                        <li><a href="{{ route('admin-dashboard') }}">Personal</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-link"></i><span>Account Settings</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('admin-profile') }}">Profile</a></li>
                        <li><a href="{{ route('admin-details') }}">Bank Account Detail</a></li>
                        <li><a href="{{ route('admin-password') }}">Change Password</a></li>
                    </ul>
                </li>
                <li class=""><a href="{{ route('admin-match') }}"><i class="fa fa-circle-o-notch"></i> <span>Matching Room</span></a></li>
                <li class=""><a href="{{ route('admin-members') }}"><i class="fa fa-users"></i> <span>Manage Members</span></a></li>
                <li class=""><a href="{{ route('admin-resolve') }}"><i class="fa fa-handshake-o"></i> <span>Resolve Conflicts</span></a></li>
                <li class=""><a href="{{ route('support') }}"><i class="fa fa-support"></i> <span>Support</span></a></li>
                <li class=""><a href="{{ route('configurations') }}"><i class="fa fa-gears"></i> <span>System Settings</span></a></li>
                <li class=""><a href="{{ route('admin-logout') }}"
                                onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out"></i><span>Logout</span></a></li>
            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content container-fluid">
            @if(session()->has('error'))
                <div class="row">
                    <div class="col col-md-6">
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                            </button>
                            <strong>{{ session()->get('error') }}</strong>
                        </div>
                    </div>
                </div>
            @endif
            @if(session()->has('success'))
                <div class="row">
                    <div class="col col-md-6">
                        <div class="alert alert-success">
                            <button class="close" type="button" aria-hidden="true">&times;
                            </button>
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    </div>
                </div>
            @endif
            @if(session()->has('processing'))
               <div class="row">
                        <div class="col col-md-6">
                            <div class="alert alert-info">
                                <button class="close" type="button" aria-hidden="true">&times;
                                </button>
                                <strong>{{ session()->get('processing') }}</strong>
                            </div>
                        </div>
                    </div>
            @endif
            @yield('systems-dashboard')
            @yield('dashboard')
            @yield('manage-members')
            @yield('manage-members-view')
            @yield('manage-members-edit')
            @yield('mass-com')
            @yield('resolve-conflict')
            @yield('support-auth')
            @yield('profile')
            @yield('account')
            @yield('password')
            @yield('history')
            @yield('configs')
            @yield('support')
            @yield('linking')
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            <strong>&copy;  {{ now()->year }}.</strong> All rights reserved.
        </div>

        <!-- Default to the left -->
        <strong><a href="{{ route('home') }}">{{ route('home') }}</a></strong>
    </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>

<!-- DataTables -->
<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<!-- page script -->
<script>
    $(function () {
        $('#example1').DataTable();
        $('#table1').DataTable();
        $('#table2').DataTable();
        $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
        })
    })
</script>

</body>
</html>
