@extends('layout.internal-main')

@section('profile')
        <!-- Profile Image -->
<div class="box box-primary">
    <div class="box-body box-profile">
        <img class="profile-user-img img-responsive img-circle" src="dist/img/p2p2/user_128.png" alt="User profile picture">

        <h3 class="profile-username text-center">{{ $user['payment_name'] }}</h3>

        <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
                <b>Phone Number</b> <a class="pull-right">{{ $user['phone'] }}</a>
            </li>
            <li class="list-group-item">
                <b>Email Address</b> <a class="pull-right">{{ $user['email'] }}</a>
            </li>
            <li class="list-group-item">
                <b>Region</b> <a class="pull-right">{{ $user['region'] }}</a>
            </li>
            <li class="list-group-item">
                <b>Current Pledge</b> <a class="pull-right">c {{ $user['deposited_amount'] }}</a>
            </li>
            <li class="list-group-item">
                <b>Last Login</b> <a class="pull-right">{{ $user['last_logged_at'] }}</a>
            </li>
        </ul>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->
    @endsection