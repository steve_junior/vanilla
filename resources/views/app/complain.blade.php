@extends('layout.internal-main')

@section('support-auth')
   <div class="row">
       <div class="col-md-8">
           <section class="content">
               <div class="box box-warning">
                   <div class="box-header with-border">
                       <h3 class="box-title">Have a concern ?  Leave us a message below</h3>
                   </div>

                   <div class="box-body">
                       <form method="post" role="form" action="{{ route('contact-support') }}">
                           {{ csrf_field() }}

                           <!--div class="form-group">
                               {{--<input type="hidden" class="form-control" value="{{ $no_of_complains_logged }}"  disabled>--}}
                           </div-->
                           <div class="form-group">
                               <label>Reason</label>
                               <input name="reason" type="text" class="form-control" placeholder="Enter ..." required>
                           </div>
                           <!-- text area -->
                           <div class="form-group">
                               <label>Comment</label>
                               <textarea name="comment" class="form-control" rows="5" placeholder="Enter ..."></textarea>
                           </div>
                           <div class="form-group">
                               <!--button type="button" class="btn btn-success" data-toggle="modal" data-target="#forward-complain-modal" tabindex="4">
                                   Send
                               </button-->
                               <button type="submit" class="btn btn-success" tabindex="4">Send</button>
                           </div>
                       </form>
                       <div class="modal fade" id="forward-complain-modal">
                           <div class="modal-dialog">
                               <div class="modal-content">
                                   <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                           <span aria-hidden="true">&times;</span></button>
                                       <h4 class="modal-title">Forward Complaint</h4>
                                   </div>
                                   <div class="modal-body">
                                       <p>Are you sure to proceed with this action?</p>
                                   </div>
                                   <div class="modal-footer">
                                       <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                                       <button type="button" class="btn btn-success" data-dismiss="modal">Proceed</button>
                                   </div>
                               </div>
                               <!-- /.modal-content -->
                           </div>
                           <!-- /.modal-dialog -->
                       </div>
                   </div>
               </div>

               <div class="box">
                   <div class="box-header with-border">
                       <h3 class="box-title">Lorem ipsum</h3>

                       <div class="box-tools pull-right">
                           <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                               <i class="fa fa-minus"></i>
                           </button>
                           <button type="button" class="btn btn-box-tool" data-widget="" data-toggle="tooltip" title="Refresh">
                               <i class="fa fa-refresh"></i>
                           </button>
                           <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                               <i class="fa fa-times"></i>
                           </button>
                       </div>
                   </div>
                   <div class="box-body">
                       Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem.
                       Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem.
                       Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem.
                       Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem.
                       Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem.
                       Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem.
                       Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem.
                   </div>
                   <!-- /.box-body -->
               </div>

           </section>
       </div>
   </div>
@endsection