@extends('layout.internal-main')

@section('account')
 <!-- Input addon -->
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Credit Account Detail</h3>
    </div>
    <div class="box-body">
        <form method="POST" class="form account-form" role="form" action = '{{ route('editDetails') }}'>
            {{ csrf_field() }}

            <div class="input-group form-group">
                <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                <label for="payment_method"></label>
                <select id="payment_method" name="payment_method" class="form-control" tabindex="1">
                    @foreach($methods as $key => $value)
                        @if($user['payment_method'] == $value)
                            <option value="{{ $value }}" selected>{{ $value }}</option>
                        @else
                            <option value="{{ $value }}">{{ $value }} </option>
                        @endif
                    @endforeach
                </select>
            </div>
            <br>
            <div class="input-group form-group">
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>

                <div class="form-group">
                    <input type="text" class="form-control" name="payment_name" value="{{ $user['payment_name'] }}" disabled>
                </div>

            </div>
            <br>
            <div class="input-group form-group">
                <span class="input-group-addon"><i class="fa fa-phone-square"></i></span>
                <input type="tel" min="10"  name="payment_number" value="{{ $user['payment_number'] }}" class="form-control">
            </div>
            <br>
            <div class="form-group">
                <button type="submit" class="btn btn-warning btn-block btn-lg" tabindex="4">
                    Submit
                </button>
            </div> <!-- /.form-group -->
            <!-- /input-group -->
        </form>

    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->
    @endsection