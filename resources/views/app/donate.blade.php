@extends('layout.internal-main')

@section('donate')
    <div class="row">
        <div class="col-md-8">
            @if($errors->any())
                <div class="alert alert-warning">
                    {{$errors->first()}}
                </div>
            @endif
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Lorem ipsum</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem.
                </div>
                <div class="box-footer">
                    <form method="POST" class="form account-form" role="form" action = '{{ route('donate') }}'>
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="donation_amount" class="placeholder-hidden">Amount</label>
                                    <input id="donation_amount" type="number" min="100" max="2000" autofocus class="form-control" value="" placeholder="" name="donation_amount">
                                </div>
                            </div>
                        </div> <!-- /.form-group -->

                        <div class="form-group">
                            <div class="pull-left">
                                <button type="submit" class="btn btn-primary btn-block" tabindex="4">
                                    Donate
                                </button>
                            </div>
                        </div> <!-- /.form-group -->
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    @endsection