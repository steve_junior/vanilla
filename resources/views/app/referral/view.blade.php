@extends('layout.internal-main')

@section('view-link')
    <section class="content">
        <div class="callout callout-success">
            <h5>Referral Link</h5>
            <h2>
                {{ $referrallink }}
            </h2>
        </div>
    </section>
@endsection()