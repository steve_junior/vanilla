@extends('layout.internal-main')

@section('list-referrals')
    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Accumulated:  <span class="label label-danger"> c   {{ (int) $data->sum('share') }}</span></h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="" data-toggle="tooltip" title="Refresh">
                        <i class="fa fa-refresh"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>S/N</th>
                            <th>Account</th>
                            <th>Email</th>
                            <th>Last Donation</th>
                            <th>Percentage</th>
                        </tr>
                        @foreach($data->toArray() as $item)
                            <tr>
                                <td>{{ $item['index'] }}</td>
                                <td>{{ $item['account'] }}</td>
                                <td>{{ $item['email'] }}</td>
                                <td>c {{ $item['last_donation'] }}</td>
                                <td>c {{$item['share'] }}</td>
                            </tr>
                        @endforeach

                        @if((int) $data->sum('share') >= 350)
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>
                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#cash-out-modal"> Cash Out </button>
                                    <div class="modal fade" id="cash-out-modal">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">Referral Bonus Maturity</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Are you sure to retrieve your referral bonus of <b>c {{ (int) $data->sum('share') }}</b> which apparently has matured ? </p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>

                                                    <form action="{{ route('cash-out') }}" method="post">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" value="{{ (int) $data->sum('share') }}" name="amount">
                                                        <button type="submit" class="btn btn-success" >Yes</button>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                </th>
                            </tr>
                        @endif
                    </table>
                </div>
                <!-- /.box-body -->
                <!-- /.box -->
            </div>
            <!-- /.box-body -->
        </div>

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Lorem ipsum</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="" data-toggle="tooltip" title="Refresh">
                        <i class="fa fa-refresh"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem.
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem.
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem.
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem.
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem.
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem.
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porttitor, mauris in imperdiet suscipit, velit dolor mattis neque, nec placerat nunc massa non quam. Duis et rhoncus sem.
            </div>
            <!-- /.box-body -->
        </div>
    </section>
@endsection