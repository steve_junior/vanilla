<?php

use App\Http\Middleware\Member;
use Illuminate\Auth\Middleware\Authenticate;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*********Debugging Routes*************/
Route::get('/welcome', function () {
    return view('welcome');
});



/*************** GUEST ROUTES ********************************/

Route::get('/', 'HomeController@index')->name('home');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/contact', 'HomeController@contact')->name('contact');

Route:: get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');

Route:: get('/register/{user_name}', 'Auth\RegisterController@showRegistrationForm')->name('register');

Route::post('/register', 'Auth\RegisterController@register')->name('register');

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');

Route::post('/login', 'Auth\LoginController@login')->name('login');

Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

/************************ ADMIN LOGIN **********************************/


Route::prefix('admin')->group(function(){

    Route::get('/login',  'Admin\LoginController@showLoginForm')->name('admin-login');

    Route::post('/login',  'Admin\LoginController@login')->name('admin-login');

    Route::post('/logout', 'Admin\LoginController@logout')->name('admin-logout');
}
);


/********************************************************************/



Route::middleware([Member::class, Authenticate::class])->group(function () {
    // App Internal Pages
    Route::get('/dashboard', 'Members\DashboardController@index')->name('member-dashboard');
    Route::get('/profile',   'Members\AccountController@profile')->name('profile');
    Route::get('/account',   'Members\AccountController@accountDetails')->name('details');
    Route::post('/account',  'Members\AccountController@updateAccountDetails')->name('editDetails');
    Route::get('/password',  function(){ return view('app.password'); })->name('password');
    Route::post('/password', 'Members\AccountController@changePassword')->name('changePassword');
    Route::get('/history',   'Members\AccountController@history')->name('history');
    Route::get('/donate',    'Members\DonationController@index')->name('donate');
    Route::post('/donate',   'Members\DonationController@donate')->name('donate');
    Route::post('/queue',    'Members\QueueingController')->name('push-to-queue');
    Route::get('/contact-support', 'Members\SupportController@index')->name('contact-support');
    Route::post('/contact-support', 'Members\SupportController@complain')->name('contact-support');
    Route::get('/view-referral-link', 'Members\ReferralController@view')->name('view-link');
    Route::get('/view-referrals', 'Members\ReferralController@index')->name('view-referrals');
    Route::post('/cash-out-bonus', 'Members\ReferralController@cashout')->name('cash-out');

    // Payments
    Route::post('/paid', 'Members\PaymentController@Paid')->name('member-paid');
    Route::post('/confirm-payment', 'Members\PaymentController@ConfirmedPayment')->name('member-confirm-payment');
    Route::post('/decline-payment', 'Members\PaymentController@DeclinedPayment')->name('member-decline-payment');
    Route::post('/provide-proof', 'Members\PaymentController@ProvidePaymentProof')->name('member-upload-proof');
});


Route::get('/password-reset', function(){
    return view('password.reset');
});

Route::get('/contact-guest', function(){
    return view('support.guest');
});



Route::middleware('auth.admin:admin')->prefix('admin')->group(function() {
    // Administrator Content
    Route::get('/dashboard', 'Admin\DashboardController@index')->name('admin-dashboard');
    Route::get('/system-dashboard', 'Admin\DashboardController@advanced')->name('admin-system-dashboard');

    Route::get('/members', 'Admin\ManageMembersController@index')->name('admin-members');
    Route::get('/members-view-{member}', 'Admin\ManageMembersController@view')->name('admin-view-member');
    Route::get('/members-edit-{member}', 'Admin\ManageMembersController@edit')->name('admin-edit-member');
    Route::post('/members-update-{member}', 'Admin\ManageMembersController@update')->name('admin-update-member');
    Route::post('/membership-confirmation', 'Admin\ManageMembersController@confirm')->name('admin-confirm-member');
    Route::post('/member-account-unblock', 'Admin\ManageMembersController@unblock')->name('admin-unbar-member');

    Route::get('/resolve-conflict', 'Admin\ConflictResolutionController@index')->name('admin-resolve');
    Route::post('/confirm', 'Admin\ConflictResolutionController@confirm')->name('admin-confirm-transaction');
    Route::post('/decline', 'Admin\ConflictResolutionController@decline')->name('admin-decline-transaction');

    Route::get('/profile', 'Admin\AccountController@profile')->name('admin-profile');
    Route::get('/account', 'Admin\AccountController@accountDetails')->name('admin-details');
    Route::post('/account', 'Admin\AccountController@updateAccountDetails')->name('admin-update-details');
    Route::get('/password', function(){ return view('admin.account.password'); })->name('admin-password');
    Route::post('/password', 'Admin\AccountController@changePassword')->name('admin-change-password');

    Route::get('/configurations', 'Admin\ConfigurationController@index')->name('configurations');
    Route::post('/configurations', 'Admin\ConfigurationController@update')->name('configurations');

    // Payments
    Route::post('/confirm-payment', 'Admin\PaymentController@ConfirmedPayment')->name('confirm-payment');
    Route::post('/decline-payment', 'Admin\PaymentController@DeclinedPayment')->name('decline-payment');

    // Support
    Route::get('/support', 'Admin\SupportController@index')->name('support');
    Route::post('/issue-resolved', 'Admin\SupportController@resolve')->name('resolve');
    Route::post('/issue-unresolved', 'Admin\SupportController@unresolve')->name('unresolve');

    //Matching
    Route::get('link', 'Admin\MatchingController@index')->name('admin-match');
    Route::post('link', 'Admin\MatchingController@match')->name('admin-match');

    Route::get('/announcements', function(){
        return view('admin.announcements');
    })->name('admin-announce');

});